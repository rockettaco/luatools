local NexusLog = {}

function NexusLog.dateParseDefault(dateStr)
	local newDate = {}
	newDate.year =		tonumber(string.match(dateStr, "/(%d-)%s"))
	newDate.month =		tonumber(string.match(dateStr, "^(%d-)/"))
	newDate.day =		tonumber(string.match(dateStr, "/(%d-)/"))
	newDate.hour =		tonumber(string.match(dateStr, "%s(%d-):"))
	newDate.minute =	tonumber(string.match(dateStr, ":(%d-):"))
	newDate.second =	tonumber(string.match(dateStr, ":(%d-) "))
	if "PM" == string.match(dateStr, " (%a-)$") then
		newDate.hour = newDate.hour + 12
	end
	return newDate
end

function NexusLog.dateSort(modA, modB)
	if modA.installDate.year < modB.installDate.year then return true end
	if modA.installDate.year > modB.installDate.year then return false end
	if modA.installDate.month < modB.installDate.month then return true end
	if modA.installDate.month > modB.installDate.month then return false end
	if modA.installDate.day < modB.installDate.day then return true end
	if modA.installDate.day > modB.installDate.day then return false end
	if modA.installDate.hour < modB.installDate.hour then return true end
	if modA.installDate.hour > modB.installDate.hour then return false end
	if modA.installDate.minute < modB.installDate.minute then return true end
	if modA.installDate.minute > modB.installDate.minute then return false end
	if modA.installDate.second < modB.installDate.second then return true end
	return false
end

function NexusLog.dateToStrISO(dateTable)
	return string.format("%d-%d-%d %d:%d:%d",
						 dateTable.year,
						 dateTable.month,
						 dateTable.day,
						 dateTable.hour,
						 dateTable.minute,
						 dateTable.second)
end

function NexusLog.extractModData(strData, dateParse, mtDate)
	if nil == dateParse then
		dateParse = function(input)
			return input
		end
	end
	local mods = {}
	local modData = string.match(strData, "<modList>(.-)</modList>")
	for m in string.gmatch(modData, "<mod.-</mod>") do
		local key = string.match(m, "key=\"(.-)\"")
		local newMod = {}
		newMod.file = string.match(m, "path=\"(.-)\"")
		newMod.version = string.match(m, "<version.+>(.-)</version>")
		newMod.name = string.match(m, "<name>(.-)</name>")
		newMod.installDate = dateParse(string.match(m, "<installDate>(.-)</installDate>"))
		setmetatable(newMod.installDate, mtDate)
		if newMod.name ~= "Dummy Mod: ORIGINAL_VALUE" then
			mods[key] = newMod
		end
	end
	return mods
end

function NexusLog.extractFileData(strData)
	local files = {}
	local fileData = string.match(strData, "<dataFiles>(.-)</dataFiles>")
	for f in string.gmatch(fileData, "<file.-</file>") do
		local newFile = {}
		newFile.path = string.match(f, "path=\"(.-)\"")
		newFile.writes = {}
		local modList = string.match(f, "<installingMods>(.-)</installingMods>")
		for k in string.gmatch(modList, "key=\"(.-)\"") do
			newFile.writes[k] = true
		end
		table.insert(files, newFile)
	end
	return files
end

function NexusLog.size(t)
	local count = 0
	for k,v in pairs(t) do
		count = count + 1
	end
	return count
end

function NexusLog:getFileConflicts()
	if self.fileConflictsValid then
		return self.fileConflicts
	end
	self.fileConflicts = {}
	for _,f in pairs(self.files) do
		if self.size(f.writes) > 1 then
			table.insert(self.fileConflicts, f)
		end
	end
	self.fileConflictsValid = true
	return self.fileConflicts
end

function NexusLog:getModConflicts()
	if self.modConflictsValid then
		return self.modConflicts
	end
	if not self.fileConflictsValid then
		self:getFileConflicts()
	end
	--this gets the conflicts as a hashset
	local conflictSet = {}
	for _,f in pairs(self.fileConflicts) do
		for k,_ in pairs(f.writes) do
			conflictSet[k] = true
		end
	end
	--convert to an array here for ease-of-use
	self.modConflicts = {}
	for k,_ in pairs(conflictSet) do
		table.insert(self.modConflicts, k)
	end
	self.modConflictsValid = true
	return self.modConflicts
end

function NexusLog:purgeMod(index)
	removedMod = self.mods[index]
	table.remove(self.mods, index)
	--can't use pairs() here; removing an entry changes following indices,
	--so the next call to iterator will skip the first moved entry
	local i = 1
	while i <= #self.files do
		self.files[i].writes[removedMod] = nil
		if 0 == self.size(self.files[i].writes) then
			table.remove(self.files, i)
		else
			i = i + 1
		end
	end
	self.fileConflictsValid = false
	self.modConflictsValid = false
	self.conflictClustersValid = false
	self.conflictGraphValid = false
	self.dgraphValid = false
end

function NexusLog:purgeFile(index)
	table.remove(self.files, index)
	self.fileConflictsValid = false
	self.modConflictsValid = false
	self.conflictClustersValid = false
	self.conflictGraphValid = false
	self.dgraphValid = false
end

function NexusLog:findMod(pattern)
	local results = {}
	for k,v in pairs(self.mods) do
		if nil ~= string.match(v.name, pattern) then
			results[k] = v
		end
	end
	return results
end

function NexusLog:findFile(pattern)
	local results = {}
	for k,v in pairs(self.files) do
		if nil ~= string.match(v.path, pattern) then
			results[k] = v
		end
	end
	return results
end

function NexusLog:conflictFilesFromMod(target)
	if type(target) == "number" then
		target = self.mods[target]
	end
	local conflicts = {}
	for _,f in pairs(self.files) do
		if nil ~= f.writes[target] and 1 < self.size(f.writes) then
			table.insert(conflicts, f)
		end
	end
	return conflicts
end

function NexusLog:conflictsWith(target)
	if type(target) == "number" then
		target = self.mods[target]
	end
	local conflicts = {}
	--this gets the conflicts as a hashset
	for _,f in pairs(self.files) do
		if nil ~= f.writes[target] and 1 < self.size(f.writes) then
			for k,_ in pairs(f.writes) do
				if k ~= target then conflicts[k] = true end
			end
		end
	end
	--convert to array
	local results = {}
	for k,_ in pairs(conflicts) do
		table.insert(results, k)
	end
	return results
end

--Groups mods linked by conflicts, ie those that must be installed in a relative ordering
function NexusLog:getConflictClusters()
	if self.conflictClustersValid then
		return self.conflictClusters
	end
	self:getModConflicts()
	local used = {}
	self.conflictClusters = {}
	for _,m in pairs(self.modConflicts) do
		if not used[m] then
			local newCluster = {}
			self:clusterHelper(used, newCluster, m)
			table.insert(self.conflictClusters, newCluster)
		end
	end

	self.conflictClustersValid = true
	return self.conflictClusters
end

function NexusLog:clusterHelper(used, cluster, target)
	for _,m in pairs(self:conflictsWith(target)) do
		if not used[m] then
			used[m] = true
			table.insert(cluster, m)
			self:clusterHelper(used, cluster, m)
		end
	end
end

function NexusLog:getConflictGraph()
	if self.conflictGraphValid then
		return self.conflictGraph
	end
	self.conflictGraph = {}
	for _,m in pairs(self.mods) do
		self.conflictGraph[m] = self:conflictsWith(m)
	end
	
	self.conflictGraphValid = true
	return self.conflictGraph
end

function NexusLog:directGraphManual()
	if self.dgraphValid then
		return self.dgraph
	end
	self.dgraph = {}
	local conflictSets = {}
	for _,m in pairs(self.mods) do
		self.dgraph[m] = {beforeThis = {}, afterThis = {}}
		conflictSets[m] = {}
		local list = self:conflictsWith(m)
		for _,c in pairs(list) do
			conflictSets[m][c] = true
		end
	end

	for mod, set in pairs(conflictSets) do 
		for conflict,_ in pairs(set) do
			conflictSets[conflict][mod] = nil
			io.write("Does \"" .. mod.name .. "\"\n\toverwrite\n\"" .. conflict.name .. "\"?\n")
			if "y" == string.lower(io.read()) then
				table.insert(self.dgraph[conflict].afterThis, mod)
				table.insert(self.dgraph[mod].beforeThis, conflict)
			else
				table.insert(self.dgraph[conflict].beforeThis, mod)
				table.insert(self.dgraph[mod].afterThis, conflict)
			end
		end
	end
	self.dgraphValid = true
	return dgraph
end

function NexusLog:topoSortGraph()

end

NexusLog.__index = NexusLog

function NexusLog.new(filename, dateParse)
	local newLog = {}
	dateParse = dateParse or NexusLog.dateParseDefault
	newLog.mtDate = {}
	newLog.mtDate.__tostring = NexusLog.dateToStrISO

	local f = io.open(filename, "r")
	local strdata = f:read("*a")
	f:close()
	
	newLog.mods = NexusLog.extractModData(strdata, dateParse, newLog.mtDate)
	newLog.files = NexusLog.extractFileData(strdata)
	
	--remap from keys to direct references to mod tables
	for _,f in pairs(newLog.files) do
		local newWrites = {}
		for m,_ in pairs(f.writes) do
			newWrites[newLog.mods[m]] = true
		end
		f.writes = newWrites
	end
	--rewrite mod table as an array for convenience, keys no longer necessary
	local newMods = {}
	for _,m in pairs(newLog.mods) do
		table.insert(newMods, m)
	end
	newLog.mods = newMods

	setmetatable(newLog, NexusLog)
	return newLog
end

return NexusLog