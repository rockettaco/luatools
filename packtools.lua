local packtools = {}

local util = require("util")

--Identify and report errors in the 
function packtools.validate(device)
	local sigGroups = {}	--Maps signals to the groups they appear in
	local pinMaps = {}		--Maps pins to the signals connected to them
	local errors = 0
	
	for k,v in pairs(device.packages) do
		pinMaps[k] = {}
	end

	for groupName, group in pairs(device.signals) do
		for sigName, sig in pairs(group) do
			--Record signal name
			sigGroups[sigName] = sigGroups[sigName] or {}
			table.insert(sigGroups[sigName], groupName)
			--Record associated pins for each package
			for pack, _ in pairs(pinMaps) do
				if nil == sig[pack] or #sig[pack] == 0 then
					print("WARNING: signal " .. sigName .. " undefined for " .. "package " .. device.packages[pack].name)
					errors = errors + 1
				else
					for _, pin in pairs(sig[pack]) do
						if false == pin then
							if #sig[pack] > 1 then
								print("WARNING: signal " .. sigName .. " in package " .. device.packages[pack].name ..
									  "specifies both unconnected (false) and valid pins")
								errors = errors + 1
							end
						elseif "" == pin then
							print("WARNING: signal " .. sigName .. "has empty pin name in package " .. device.packages[pack].name)
							errors = errors + 1
						else
							pinMaps[pack][pin] = pinMaps[pack][pin] or {}
							table.insert(pinMaps[pack][pin], {signal = sigName, group = groupName})
						end
					end
				end
			end
		end
	end

	--Report duplicate signal names
	for sigName, groupList in pairs(sigGroups) do
		if #groupList > 1 then
			print("WARNING: signal " .. sigName .. " appears multiple times:")
			for k,v in pairs(groupList) do
				print("\t" .. sigName .. " in group " .. v)
			end
			errors = errors + 1
		end
	end

	--Report duplicate pins
	for pack, pinList in pairs(pinMaps) do
		for pin, sigList in pairs(pinList) do
			if #sigList > 1 then
				print("WARNING: pin " .. pin .. " appears multiple times in " .. "package " .. device.packages[pack].name .. ":")
				for k,v in pairs(sigList) do
					print("\t" .. tostring(v.signal) .. " in group " .. tostring(v.group))
				end
				errors = errors + 1
			end
		end
	end
	print("Done validating.")
	return errors
end

--[[Comparator function for sorting BGA pins by location. In this method pins
	are ordered first by alpha row, then within rows by numeric column.
--]]
--[[
function packtools.compPinBGA(a, b)
	a = {string.byte(a, 1, string.len(a))}
	b = {string.byte(b, 1, string.len(b))}
	table.insert(a, 0)
	table.insert(b, 0)

	for
end
--]]

--[[Gets the index number of a package variant from the package name. Returns
	false if no package with the specified name is defined.
--]]
function packtools.getPackage(device, packName)
	for i, pack in pairs(device.packages) do
		if pack.name == packName then
			return i
		end
	end
	return false
end

--[[Gets the index number of a package variant from the variant tag code.
	Returns false if no package with the specified variant tag exists.
--]]
function packtools.getVariant(device, varTag)
	for i, pack in pairs(device.packages) do
		if pack.variant == varTag then
			return i
		end
	end
	return false
end

--Count the number of pins used by a package variant in a symbol
local function countPinsSymbol(symbol, pack)
	local count = 0
	for sigName, signal in pairs(symbol) do
		if false ~= signal[pack][1] then
			count = count + #signal[pack]
		end
	end
	return count
end

--Count the number of used pins in a package variant
function packtools.countPins(device, pack, symbol)
	if nil ~= symbol then
		return countPinsSymbol(device.signals[symbol], pack)
	end
	local count = 0
	for symName, sym in pairs(device.signals) do
		count = count + countPinsSymbol(sym, pack)
	end
	return count
end

--[[Search for a pin and report the signal assigned to it and the symbol in
	which the signal appears. Returns false if the pin could not be found.
--]]
function packtools.findPin(device, pin, pack)
	for symName, symbol in pairs(device.signals) do
		for sigName, signal in pairs(symbol) do
			for _, pinName in pairs(signal[pack]) do
				if pinName == pin then
					return sigName, symName
				end
			end
		end
	end
	return false
end

--Count the number of signals used by a package variant in a symbol
local function countSignalsSymbol(symbol, pack)
	local count = 0
	for sigName, signal in pairs(symbol) do
		if false ~= signal[pack][1] then
			count = count + 1
		end
	end
	return count
end

--Count the number of signals used by a device
function packtools.countSignals(device, pack, symbol)
	if nil ~= symbol then
		return countSignalsSymbol(device.signals[symbol], pack)
	end
	local count = 0
	for symName, sym in pairs(device.signals) do
		count = count + countSignalsSymbol(sym, pack)
	end
	return count
end

--[[Search for a signal and give the pin assignments and description or notes
	associated with it. The symbol name where the pin was found is also
	returned.
--]]
function packtools.findSignal(device, signal)
	for symName, symbol in pairs(device.signals) do
		for sigName, sig in pairs(symbol) do
			if signal == sigName then
				return sig, symName
			end
		end
	end
	return false
end

--[[Identify any signals not supported by a device variant. The returned
	structure is a mapping of symbol names to lists of signals in those symbols
	which the specified variant does not support. Symbols with no such signals
	are omitted
--]]
function packtools.getMissingSignals(device, variant)
	local sigMap = {}
	local missing
	
	for symName, symbol in pairs(device.signals) do
		missing = {}
		for sigName, signal in pairs(symbol) do
			if false == signal[variant][1] then
				table.insert(missing, sigName)
			end
		end
		if #missing > 0 then
			sigMap[symName] = missing
		end
	end
	return sigMap
end

--Determine whether a package implements any of the signals in a symbol
function packtools.supportsSymbol(symbol, pack)
	for sigName, signal in pairs(symbol) do
		if signal[pack][1] ~= false then
			return true
		end
	end
	return false
end

--[[Gets groups of packages which share a common version of a symbol. The result
	is a map from symbol names to one or more lists, each of which contains
	package variants that have the same signals and pin counts per signal.
	
	There are faster ways to do this, but the time loss is minimal and this
	method is clearer.
--]]
function packtools.groupSymbols(device)
	local symMap = {}
	local placed
	local compatible
	local pinsPack
	local pinsList
	for symName, symbol in pairs(device.signals) do
		symMap[symName] = {}
		for pack, _ in pairs(device.packages) do
			if not packtools.supportsSymbol(symbol, pack) then
				symMap[symName][false] = symMap[symName][false] or {}
				table.insert(symMap[symName][false], pack)
			else
				--Scan for existing groups with the same signal counts
				placed = false
				for _, packList in pairs(symMap[symName]) do
					compatible = true
					--Check each signal
					for sigName, signal in pairs(symbol) do
						pinsPack = signal[pack]
						pinsList = signal[packList[1]]
						if #pinsPack ~= #pinsList or (false == pinsPack[1]) ~= (false == pinsList[1]) then
							compatible = false
							break
						end
					end
					--Add to group
					if compatible then
						placed = true
						table.insert(packList, pack)
						break
					end
				end
				--Make new group
				if not placed then
					table.insert(symMap[symName], {pack})
				end
			end
		end
	end
	return symMap
end

--[[Gets list of all compatible (same signals and pin counts) packages in a
	device and the packages that share the version of each symbol.
--]]
function packtools.groupPackages(device)
	local symMap = packtools.groupSymbols(device)
	local packMap = {}
	--reverse symbol=>package map to package=>symbol map
	for symName, groupList in pairs(symMap) do
		for groupID, group in pairs(groupList) do
			for _, pack in pairs(group) do
				packMap[pack] = packMap[pack] or {}
				--TODO: handle groupID == false
				if false == groupID then
					packMap[pack][symName] = false
				else
					packMap[pack][symName] = group
				end
			end
		end
	end
	--rebuild with list keys, merging similar packages
	local mergedMap = {}
	local placed
	local compatible
	for pack, symListNew in pairs(packMap) do
		placed = false
		--compare all symbol versions in each entry
		for packList, symListTest in pairs(mergedMap) do
			compatible = true
			for symName, version in pairs(symListNew) do
				if version ~= symListTest[symName] then
					compatible = false
					break
				end
			end
			if compatible then
				placed = true
					table.insert(packList, pack)
				break
			end
		end
		if not placed then
			mergedMap[{pack}] = symListNew
		end
	end
	return mergedMap
end

--Functions for exporting package definitions to EAGLE CAD library files
packtools.eagle = {}

--Export each symbol in the device to a set of EAGLE CAD library symbols
function packtools.eagle.exportSymbols(device, file)
	local symMap = packtools.groupSymbols(device)
	local nameList
	local name
	local offset
	local pinList
	local decorator
	for symName, symbol in pairs(symMap) do
		for _, packList in ipairs(symbol) do --exclude "false" key (unsupported)
			--get an ordered list of signal names in the symbol
			nameList = {}
			for sigName, signal in pairs(device.signals[symName]) do
				if false ~= signal[ packList[1] ] then
					table.insert(nameList, sigName)
				end
			end
			table.sort(nameList, util.natStrComp)
			
			name = string.upper(device.name) .. "_" .. string.upper(symName)
			
			--if this is not the only symbol variant, tag it with the packages
			if (#symbol + ( (symbol[false] ~= nil) and 1 or 0) ) > 1 then
				for _, v in pairs(packList) do
					name = name .. "_" .. device.packages[v].variant
				end
			end
			
			--write the symbol to file
			offset = 0
			file:write(string.format('<symbol name="%s">\n', name))
			for _, sigName in ipairs(nameList) do
				pinList = device.signals[symName][sigName][ packList[1] ]
				decorator = #pinList > 1 and
					function(n) return "@" .. tostring(n) end
					or
					function(n) return "" end
				if pinList[1] ~= false then
					for i = 1, #pinList do
						file:write(string.format('<pin name="%s" x="0" y="%.2f" visible="pin" length="short"/>\n', string.upper(sigName) .. decorator(i), offset * -2.54))
						offset = offset + 1
					end
				end
			end
			file:write(string.format('<text x="0" y="%s" size="1.5" layer="95">&gt;NAME</text>\n', offset * -2.54))
			file:write(string.format('<text x="0" y="%s" size="1.5" layer="96">&gt;VALUE</text>\n', (offset + 1) * -2.54))
			file:write('</symbol>\n')
		end
	end
end

--Export the pin mappings to a set of EAGLE CAD library devices
function packtools.eagle.exportDevice(device, file)
	--find each group of packages that share all symbol versions
	local commonPacks = packtools.groupPackages(device)
	local name
	
	for packList, symList in pairs(commonPacks) do
		name = device.name
		if #packList ~= #device.packages then
			for _, pack in pairs(packList) do
				name = name .. "_" .. string.upper(device.packages[pack].variant)
			end
		end
		file:write(string.format('<deviceset name="%s" prefix="U">\n', name))
		
		--write symbols
		file:write('<gates>\n')
		for symName, symPacks in pairs(symList) do
			if false ~= symPacks then
				name = string.upper(device.name) .. "_" .. string.upper(symName)
				if #symPacks ~= #device.packages then
					for _, pack in pairs(symPacks) do
						name = name .. "_" .. string.upper(device.packages[pack].variant)
					end
				end
				file:write(string.format('<gate name="%s" symbol="%s" x="0" y="0"/>\n', string.upper(symName), name))
			end
		end
		file:write('</gates>\n')
		
		--make connections for each device
		file:write('<devices>\n')
		for _, pack in pairs(packList) do
			file:write(string.format('<device name="%s" package="%s">', device.packages[pack].variant, device.packages[pack].exportName))
			file:write('<connects>\n')
			
			for symName, symPacks in pairs(symList) do
				for sigName, pinList in pairs(device.signals[symName]) do
					if false ~= pinList[pack][1] then
						decorator = #pinList[pack] > 1 and
							function(n) return "@" .. tostring(n) end
							or
							function(n) return "" end
						for i, pin in ipairs(pinList[pack]) do
							file:write(string.format('<connect gate="%s" pin="%s" pad="%s"/>\n', string.upper(symName), sigName .. decorator(i), pin))
						end
					end
				end
			end
			
			file:write('</connects>\n')
			file:write('<technologies>\n')
			file:write('<technology name=""/>\n')
			file:write('</technologies>\n')
			file:write('</device>\n')
		end
		
		file:write('</devices>\n')
		file:write('</deviceset>\n')
	end
end

--Functions for exporting package definitions to KiCAD library files
packtools.kicad = {}

--[[Export each version of the device symbols and pin mappings to a set of KiCAD
	library symbols
--]]
function packtools.kicad.export(device, file)
	local symbolList
	local name
	local sigList
	for pack = 1, #device.packages do	--kicad uses separate symbols for EVERY package - stupid, but we're stuck with it
		--Get and sort the supported symbols for the package
		symbolList = {}
		for symName, symbol in pairs(device.signals) do
			if packtools.supportsSymbol(symbol, pack) then
				table.insert(symbolList, symName)
			end
		end
		table.sort(symbolList, util.natStrComp)

		name = device.name .. '_' .. device.packages[pack].variant

		--Write out the device to KiCAD library format
		file:write('#\n')
		file:write('# ' .. name .. '\n')
		file:write('#\n')
		file:write(string.format('DEF %s U 0 40 Y Y %d L N\n', name, #symbolList))
		--TODO: Fx definitions
		file:write('F0 "U" 0 -50 60 H V C CNN\n')
		file:write(string.format('F1 "%s" 0 -150 60 H V C CNN\n', name))
		file:write('F2 "" 0 -250 60 H I C CNN\n')
		file:write('F3 "" 0 -250 60 H I C CNN\n')

		file:write('DRAW\n')
		
		for symNum, symName in ipairs(symbolList) do
			--ID and sort the supported pins
			sigList = {}
			for sigName, signal in pairs(device.signals[symName]) do
				if false ~= signal[pack][1] then
					table.insert(sigList, sigName)
				end
			end
			table.sort(sigList, util.natStrComp)

			offset = 0
			for _, sigName in pairs(sigList) do
				for _, pin in pairs(device.signals[symName][sigName][pack]) do
					file:write(string.format('X %s %s 0 %s 200 R 50 50 %d 1 U\n', sigName, pin, 50 + (100 * offset), symNum))
					offset = offset + 1
				end
			end
		end

		file:write('ENDDRAW\n')
		file:write('ENDDEF\n')
	end
end

return packtools