powers={
	amd_3900x        = 168,
	msi_1080_ek      = 180,
	evga_3080ti_ftw3 = 430,
}

delta_10c={
	acool_st30_push={
		[750]  = 174.7,
		[1300] = 262.4,
	},
	acool_st30_pushpull={
		[750]  = 193.4,
		[1300] = 290.4,
	},
	hwlabs_sr2_push={
		[750]  = 193.2,
		[1300] = 302.3,
	}
}

function calcDelta(power_total, delta_10c_total)
	return 10 * (power_total / delta_10c_total)
end