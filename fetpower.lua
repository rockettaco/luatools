PSMN1R2_30YLC = {
	rds_on = {
		[4.5] = {
			typical = 1.35 / 1000,
			maximum = 1.65 / 1000,
		},
		[10] = {
			typical = 1.05 / 1000,
			maximum = 1.25 / 1000,
		},
	},
	temp_factors = {
		[4.5] = {
			[-30] = 0.83,
			[25]  = 1,
			[150] = 1.69,
		},
		[10] = {
			[-30] = 0.82,
			[25]  = 1,
			[150] = 1.62,
		},
	},
}

ADC_generic = {
	reference  = 3.3,
	resolution = 12,
}

config = {
	parallel = 4,
	gate_drive = 10,
	binning = "typical",
	power = 2,   --per FET
	current = 25, --total
	amplify = 20,
}

function getTrueRDS(cfg, fet)
	local true_rds = {}
	local rds_base = fet.rds_on[cfg.gate_drive][cfg.binning]

	for k,v in pairs(fet.temp_factors[cfg.gate_drive]) do
		true_rds[k] = v * rds_base
	end

	return true_rds
end

function getVDrop(cfg, fet)
	local result = getTrueRDS(cfg, fet)
	local current_per = cfg.current / cfg.parallel

	for k,v in pairs(result) do
		result[k] = v * current_per
	end

	return result
end

function getADCRes(cfg, fet, adc)
	local result = getTrueRDS(cfg, fet)
	local ohm_per_step = (adc.reference * cfg.parallel) / (math.pow(2, adc.resolution) * cfg.amplify)

	for k,v in pairs(result) do
		result[k] = v / ohm_per_step
	end
	
	return result
end

function getPower(cfg, fet)
	local result = getTrueRDS(cfg, fet)
	local current_factor = math.pow(cfg.current / cfg.parallel, 2)
	
	for k,v in pairs(result) do
		result[k] = v * current_factor
	end
	
	return result
end

function getCurrent(cfg, fet)
	local result = getTrueRDS(cfg, fet)
	
	for k,v in pairs(result) do
		result[k] = math.sqrt(cfg.power / v) * cfg.parallel
	end

	return result
end

local function chartVariant(cfg, fet, adc, variant, current)
	--get the data together
	local binning_stash = cfg.binning
	local current_stash = cfg.current
	cfg.binning = variant
	cfg.current = current
	local voltage = getVDrop(cfg, fet)
	local adc_res = getADCRes(cfg, fet, adc)
	cfg.binning = binning_stash
	cfg.current = current_stash

	--print header
	print(string.format("%s @ %dA", variant, current))
	print("        SIGNAL      OUTPUT      RESOLUTION")
	for k,v in pairs(voltage) do
		io.write(string.format("%3dC    %#5.2f mV   %#5.2f mV   %#5.2f b", k, v * 1000, v * 1000 * cfg.amplify, adc_res[k]))
		io.write("\n")
	end
end

function chartAll(cfg, fet, adc, standard, max)
	chartVariant(cfg, fet, adc, "typical", standard)
	io.write("\n")
	chartVariant(cfg, fet, adc, "typical", max)
	io.write("\n")
	chartVariant(cfg, fet, adc, "maximum", standard)
	io.write("\n")
	chartVariant(cfg, fet, adc, "maximum", max)
	io.write("\n")
end


