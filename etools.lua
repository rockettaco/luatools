etools={}
etools.res={}
etools.cap={}

etools.res.vals={
	1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0,
	3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1
}

etools.res.decades={
	1.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0
}

function etools.res.getRange(min, max)
	local resistors = {}
	for kd,d in pairs(etools.res.decades) do
		for kv,v in pairs(etools.res.vals) do
			res = d * v
			if min < res and res < max then table.insert(resistors, res) end
		end
	end
	return resistors
end

function etools.res.format(v, decimals)
	if nil == decimals then decimals = 1 end
	if math.abs(v) >= 1000000 then
		local suffix = "M"
		v = v / 1000000
	elseif math.abs(v) >= 1000 then
		local suffix = "k"
		v = v / 1000
	elseif math.abs(v) < 1 and v ~= 0 then
		local suffix = "m"
		v = v * 1000
	else
		local suffix = ""
	end
	if 0 == v % 1 then decimals = 0 end
	return string.format("%." .. tostring(decimals) .. "f" .. suffix, v)
end

etools.cap.vals={
	1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2
}

etools.vdiv = {}

function etools.vdiv.factor(highRes, lowRes)
	return (highRes + lowRes) / lowRes
end

function etools.vdiv.highRes(factor, lowRes)
	return lowRes * (factor - 1)
end

function etools.vdiv.lowRes(factor, highRes)
	return highRes / (factor - 1)
end

--[[ Argument names:
	args.factor		Number		The voltage division factor
	args.highRes	Number		The value of the high-side resistor
	args.lowRes		Number		The value of the low-side resistor
	args.input		Number		The input voltage to the divider
	args.output		Number		The output voltage from the divider

	This function will return the argument table filled in to the maximum
	possible, or nil if there is an error.

	Passing this function any three arguments will fill in the remaining.
	Passing four arguments will cause an error if their values produce
	conflicting results and should be avoided.
	If only two arguments are passed:
		-If they are a highRes/lowRes or input/output pair, only factor will
		 be filled in.
		-If factor and one element from either the highRes/lowRes or
		 input/output pairs are provided, the other member of that pair will be
		 filled in.
		-If one element each from the highRes/lowRes and input/output pairs are
		 provided, there is not enough information and an error will result.

--]]
function etools.vdiv.calc(args)
	--Find the division factor
	if nil ~= args.highRes and nil ~= args.lowRes then
		local f = etools.vdiv.factor(args.highRes, args.lowRes)
		if nil ~= factor and factor ~= f then
			error("conflicting values")
			return nil
		end
		args.factor = f
	end
	if nil ~= args.input and nil ~= args.output then
		local f = args.input / args.output
		if nil ~= factor and factor ~= f then
			error("conflicting values")
			return nil
		end
		args.factor = f
	end
	if nil == args.factor then
		error("insufficient arguments")
		return nil
	end

	--Fill in the other arguments
	if nil == args.highRes and nil ~= args.lowRes then
		args.highRes = etools.vdiv.highRes(args.factor, args.lowRes)
	end
	if nil == args.lowRes and nil ~= args.highRes then
		args.lowRes = etools.vdiv.lowRes(args.factor, args.highRes)
	end
	if nil == args.input and nil ~= args.output then
		args.input = args.output * args.factor
	end
	if nil == args.output and nil ~= args.input then
		args.output = args.input / args.factor
	end
	
	return args
end

--[[ Argument names:
	args.factor		Number		The ideal division factor
	args.minRes		Number		The smallest allowable resistor value
	args.maxRes		Number		The largest allowable resistor value
	args.noLow		Boolean		If true, will seek the closest division factor
								that is AT LEAST as large as factor, even if
								one less than factor is closer - NOT IMPLEMENTED
--]]
function etools.vdiv.closestRes(args)
	if nil == args.minRes then args.minRes = 0 end
	if nil == args.maxRes then args.maxRes = math.huge end
	print(args.minRes)
	print(args.maxRes)
	resistors = etools.res.getRange(args.minRes, args.maxRes)

	--Check all divider combinations and find the closest one
	local bestHi = nil
	local bestLo = nil
	local bestDelta = math.huge
	for lk,lv in pairs(resistors) do
		for hk, hv in pairs(resistors) do
			delta = math.abs(etools.vdiv.factor(hv, lv) - args.factor)
			if delta < bestDelta then
				bestDelta = delta
				bestHi = hv
				bestlo = lv
			end
		end
	end
	return {high=hv, low=lv}
end

--[[ Argument names:
	args.R_fb    Feedback resistance, connecting comparator output to positive input
	args.R_hi    High-side resitance of the bias voltage divider
	args.R_lo    Low-side resitance of the bias voltage divider
	args.R_in    Resistance connecting bias voltage divider to positive input
	args.V_d     Voltage supplying bias voltage divider
	args.V_oh    High-level output resistance of comparator
	args.V_ol    Low-level output resistance of comparator
--]]		
function etools.calcComparatorHyst(args)
	local a = 1/((1/args.R_in) + (1/args.R_hi) + (1/args.R_lo))
	local hi_num = args.V_oh + ((args.R_fb * a * args.V_d) / (args.R_fb * args.R_hi))
	local lo_num = args.V_ol + ((args.R_fb * a * args.V_d) / (args.R_in * args.R_hi))
	local denom = args.R_in + args.R_fb - ((args.R_fb * a) / args.R_in)
	denom = denom * (1/args.R_in)
	return {hi_num / denom, lo_num / denom}
end


return etools
