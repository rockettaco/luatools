elements={
	[1]={
		name   = "Leg",
		qty    = 4,
		length = 40,
		stock  = "2x4",
	},
	[2]={
		name   = "Table Long",
		qty    = 2,
		length = 72,
		stock  = "2x6",
	},
	[3]={
		name   = "Table Short",
		qty    = 4,
		length = 21,
		stock  = "2x6",
	},
	[4]={
		name   = "Table Doubler",
		qty    = 2,
		length = 17,
		stock  = "2x6",
	},
	[5]={
		name   = "Shelf Long",
		qty    = 2,
		length = 72,
		stock  = "2x4",
	},
	[6]={
		name   = "Shelf Short",
		qty    = 4,
		length = 21,
		stock  = "2x4",
	},
	[7]={
		name   = "Shelf Doubler",
		qty    = 2,
		length = 17,
		stock  = "2x4",
	},
	[8]={
		name   = "Rear Brace",
		qty    = 1,
		length = 73.5,
		stock  = "2x4",
	},
	[9]={
		name   = "Side Brace",
		qty    = 2,
		length = 30.4,
		stock  = "2X4",
	},
}

stock={
	[1]={
		size   = "2x4",
		length = 96,
		qty    = 6,
	}
	[2]={
		size   = "2x4",
		length = 35.5,
		qty    = 2,
		waste  = true,
	}
	[3]={
		size   = "2x4",
		length = 23.5,
		qty    = 1,
		waste  = true,
	}
	[3]={
		size   = "2x6",
		length = 96,
		qty    = 6,
	}
}

function lumberize_naive(elements, stock)
	
end















