local base_string = 
[[
G21 G90
G00 Z10
G00 X3 Y-10
]]

local pass_sequence = 
[[
M03
G01 Y40
M05
G00 Z10
G91
G00 X6 Y-50
G90
]]

local function genPasses(passes)
	local seq = {base_string}
	for _,pass in ipairs(passes) do
		table.insert(seq, string.format("\nS%f F%f G00 Z%f\n", pass.speed, pass.feed, 0 - pass.depth))
		table.insert(seq, pass_sequence)
	end
	table.insert(seq, M00)
	return table.concat(seq)
end

local function make(dest, src)
	passes = dofile(src)

	f = io.open(dest, "w+")
	if fout == nil then error("Could not open destination file") end
	f:write(genPasses(passes))
	
	f:close();
end

return make
