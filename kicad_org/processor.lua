function bom.process(filename)
	local board_in = dofile(filename)
	
	for _, c in board.components do
		bom.clearScreen()
		
	end
end

function bom.clearScreen()
	if not os.execute("clear") then
		os.execute("cls")
	end
end

function bom.renderComp(comp)
	local name_length = 0
	for k,v in pairs(comp) do
		if #k > name_length then name_length = k end
	end
	
	local format_str = string.format("%%-%ds: %%s", name_length + 1)
	for k,v in pairs(comp) do
		print(string.format(format_str, k, v))
	end
end