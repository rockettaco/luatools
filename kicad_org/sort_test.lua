return {
	{
		field={
			"value"
		},
		ordering_func = function(a,b)
			local valid, a_val = pcall(function() return bom:parseValue(a) end)
			if not valid then return false end
			local valid, b_val = pcall(function() return bom:parseValue(b) end)
			if not valid then return true end
			if a_val.magnitude < b_val.magnitude then return true else return false end
		end
	},
	{
		field={
			"package"
		},
		ordering_func = function(a,b)
			if a < b then return true else return false end
		end
	},
	{
		field={
			"power",
		},
		ordering_func = function(a,b)
			local valid, a_val = pcall(function() return bom:parseValue(a) end)
			if not valid then return false end
			local valid, b_val = pcall(function() return bom:parseValue(b) end)
			if not valid then return true end
			if a_val.magnitude < b_val.magnitude then return true else return false end
		end
	}
}