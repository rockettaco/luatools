#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Generate a Tab delimited list (csv file type).
    Components are sorted by ref and grouped by value with same footprint
    Fields are (if exist)
    'Ref', 'Qnty', 'Value', 'Cmp name', 'Footprint', 'Description', 'Vendor'

    Command line:
    python "pathToFile/bom_csv_grouped_by_value_with_fp.py" "%I" "%O.csv"
"""

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import csv
import sys

# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
net = kicad_netlist_reader.netlist(sys.argv[1])

# Open a file to write to, if the file cannot be opened output to stdout
# instead
try:
    f = open(sys.argv[2] + ".lua", 'w')
except IOError:
    e = "Can't open output file for writing: " + sys.argv[2]
    print(__file__, ":", e, sys.stderr)
    f = sys.stdout

f.write('return {\n')
f.write('\tname="' + sys.argv[2] + '",\n')
f.write('\tsource="' + net.getSource() + '",\n')
f.write('\tdate="' + net.getDate() + '",\n')

# Get all of the components in groups of matching parts + values
# (see ky_generic_netlist_reader.py)
grouped = net.groupComponents()

f.write('\tcomponents={\n')
# Output all of the component information
for group in grouped:
    refs = ""

    # Add the reference of every component in the group and keep a reference
    # to the component so that the other data can be filled in once per group
    for component in group:
        refs += '"' + component.getRef() + '", '
        c = component

    # Fill in the component groups common data
    f.write('\t\t{value="' + c.getValue() + '", name="' + c.getPartName() + '", package="' + c.getFootprint() + '", qty="' + str(len(group)) + '", refs="' + refs + '"},\n')

f.write('\t}\n')
f.write('}\n')
f.close()