bom={}

--Deep compare of components by attribute values, ignoring .qty
local function identical(a,b)
	local item_count = 0
	for k,v in pairs(a) do
		item_count = item_count + 1
		if "qty" ~= k then
			if b[k] == nil or b[k] ~= v then return false end
		end
	end
	for _,_ in pairs(b) do item_count = item_count - 1 end
	return item_count == 0
end

--Performs one-level-deep copy of table
local function flatCopy(x)
	local copy = {}
	for k,v in pairs(x) do
		copy[k] = v
	end
	return copy
end

--[[
	This bit of hackery deals with text files that store Greek symbols as Unicode instead of
	extended ASCII. These symbols will appear correct in a text editor, but process as two
	garbage bytes when run. Since these symbols CAN be represented in ASCII, we can remap them.
	The keys in this table are the symbol in question, and as such will appear right in an editor
	but will process to the same garbage. We can, however, map this to the garbage in the data,
	and use it to replace them with the escape sequence on the right, which is the ASCII value
	that represents the same symbol.
--]]
bom.symbol_alt_map={
	["Ω"] = "\xEA",
	["µ"] = "\xE6",
	["ß"] = "\xE1",
}

--This function performs the remapping from Unicode-derived garbage as described in bom.symbol_alt_map
function bom:processBadGreek(input)
	for garbage, fix in pairs(self.symbol_alt_map) do
		input = string.gsub(input, garbage, fix) or input
	end
	return input
end

--[[
	Add all boards in a BOM to a new unified parts list.
	Expected BOM structure is:
	{
		board_1={
			desc="Board Description"
			qty=n,
			partslist={} --see condenseToLib for description
			}
		}
		...board_n
	}
--]]
function bom:condense(bom)
	local comp_lib = {}
	local found
	for _, board in pairs(bom) do --scan boards
		self.condenseToLib(comp_lib, board.parts, board.qty)
	end
	return comp_lib
end

--[[
	Add the contents of a parts list to a unified parts list.
	This will merge any matching components.
	Expected BOM structure is:
	{
		class_1={ --such as capacitor, resistor, etc
			{..., qty=n} --component 1
			...
			{} --component n
		}
		...class_n
	}
--]]
function bom.condenseToLib(lib, bom, qty)
	local found
	for classname, class in pairs(bom) do --scan component classes
		lib[classname] = lib[classname] or {} --scan components
		--compare to existing components
		for _, board_comp in pairs(class) do
			found = false
			for _, lib_comp in pairs(lib[classname]) do
				--if a match, then add quantity to lib
				if identical(board_comp, lib_comp) then
					lib_comp.qty = lib_comp.qty + (board_comp.qty * qty)
					found = true
				end
			end
			--if no match found, create as new component
			if not found then
				local new_comp = flatCopy(board_comp)
				new_comp.qty = new_comp.qty * qty
				table.insert(lib[classname], new_comp)
			end
		end
	end
end

bom.prefix_mult={
	["p"] = 1E-12,
	["n"] = 1E-9,
	["u"] = 1E-6,
	["m"] = 1E-3,
	[""]  = 1,
	["k"] = 1E3,
	["M"] = 1E6,
}

bom.prefix_range={
	{prefix="p", scale=1E-12},
	{prefix="n", scale=1E-9},
	{prefix="\xE6", scale=1E-6}, --Ω
	{prefix="m", scale=1E-3},
	{prefix="", scale=1},
	{prefix="k", scale=1E3},
	{prefix="M", scale=1E6},
}

bom.unit_symbol={
	["F"] = {name="Farad", symbol="F"},
	["\xEA"] = {name="Ohm", symbol="Ω"}, --Ω
	["V"] = {name="Volt", symbol="V"},
	["A"] = {name="Ampere", symbol="A"},
	["W"] = {name="Watt", symbol="W"},
}

--Get a pattern used to parse component value strings to their component parts
function bom:getParseValuePattern()
	if nil == self.parseValuePattern then
		local prefix_str = ""
		for k,_ in pairs(self.prefix_mult) do
			prefix_str = prefix_str .. k
		end
		local unit_str = ""
		for k, _ in pairs(self.unit_symbol) do
			unit_str = unit_str .. k
		end
		self.parseValuePattern = "([%d%.]+)([" .. prefix_str .. "]?)([" .. unit_str .. "])"
	end
	return self.parseValuePattern
end

--Get a numerical representation of a value suffixed with SI prefix and unit
function bom:parseValue(str)
	local number, prefix, unit = string.match(str, self:getParseValuePattern())
	if nil == number then
		error("String is not a valid value: " .. str)
	end
	return {
		magnitude = number * self.prefix_mult[prefix],
		unit = self.unit_symbol[unit],
	}
end

--Render a value to a conventional representation using SI prefix and unit suffixes
function bom:renderValue(val)
	local prefix_range = self.prefix_range
	for i = 1, #prefix_range do
		if val.magnitude > prefix_range[i].scale and not prefix_range[i+1] or val.magnitude < prefix_range[i+1].scale then
		   return tostring(val.magnitude / prefix_range[i].scale) .. prefix_range[i].prefix .. val.unit.symbol
		end
	end
end


--Preferred display order of attributes
local attr_order={
	"qty",
	"type",
	"value",
	"package"
}

--[[
	List-based sort comparator function for attribute names
	Using this with table.sort() will result in any values in that
	table that match those in the provided list being sorted to match
	the order of the list, followed by all attributes which do NOT
	appear in the sort list in undetermined order.
--]]
local function getAttributeComparator(order_list)
	return function(a,b)
		for _, v in ipairs(order_list) do
			if b == v then return false end
			if a == v then return true end
		end
		return false
	end
end

local function getListComparator(order_list, field_name)
	return function(a,b)
		for _, v in ipairs(order_list) do
			if b[field_name] == v then return false end
			if a[field_name] == v then return true end
		end
		return false
	end
end

--[[
	Group and order all numerical-keyed subtables in a table by
	the value of a specified field in each subtable. Records will be
	grouped by the field value, and the output order will reference
	a preferred order if one is given.
	
	recursive: ordered heirachically with order given as follows:
	field_order_list={
		{	
			field={
				"subfield_A",
			},
			ordering_func = function(),
		},
		{	
			field={
				"subfield_B",
				"subfield_C",
			},
			ordering_func = function(),
		}
		...
	}
	
	this will group the subtables by .subfield_A, then sort each group by .subfield_B.subfield_C, and so on
--]]
function bom:sortByHeirarchy(parent_table, field_order_list, index)
	--handle recursion: check where we are in the ordering heirarchy
	index = index or 1
	if index > #field_order_list then return parent_table end
	local field = field_order_list[index].field
	local ordering_func = field_order_list[index].ordering_func
	
	--map possible sub-table field values to lists of the subtables using them
	local field_values_map = {}
	local val
	for _, subtab in ipairs(parent_table) do
		val = subtab
		for _, v in ipairs(field) do
			val = val[v]
		end
		field_values_map[val] = field_values_map[val] or {}
		table.insert(field_values_map[val], subtab)
	end
	
	--transform the map's key set to an array for sorting keys
	local field_values_array = {}
	for val, _ in pairs(field_values_map) do
		table.insert(field_values_array, val)
	end
	
	--sort the array according to the ordering function
	if nil ~= ordering_func then
		table.sort(field_values_array, ordering_func)
	end
	
	--reorder parent table
	local result = {}
	local subtable_list
	for _, value in ipairs(field_values_array) do
		--sort all subtables with the same value by the next tool in the heirarchy
		subtable_list = self:sortByHeirarchy(field_values_map[value], field_order_list, index + 1)
		for _, subtable in ipairs(subtable_list) do
			table.insert(result, subtable)
		end
	end
	return result
end

--Sort comparator function for components sorting by value magnitude
local function valueComparator(a,b)
	if a.value.magnitude < b.value.magnitude then return true else return false end
end

--Display a component library in list format
function bom.present(lib)
	for classname, class in pairs(lib) do
		print("========" .. string.upper(classname) .. "========")
		--gather the attribute names
		local attr_dic = {}
		local attr_list = {}
		for _, comp in pairs(class) do
			for attr, _ in pairs(comp) do attr_dic[attr] = true end
		end
		for key, _ in pairs(attr_dic) do
			table.insert(attr_list, key)
		end
		table.sort(attr_list, getAttributeComparator(attr_order))
		--Create strings for all components, then build them together one attribute at a time
		--This makes calculating padding easier and guarantees	 the column order
		local comp_str = {}
		for key, comp in pairs(class) do
			comp_str[key] = ""
		end
		
		local longest_str = 0
		local length
		local format_str
		for _, attr in ipairs(attr_list) do
			format_str = string.format("%%-%ds %%s: %%s", longest_str)
			for key, comp in ipairs(class) do
				comp_str[key] = string.format(format_str, comp_str[key], attr, comp[attr])
				length = string.len(comp_str[key])
				if length > longest_str then longest_str = length end
			end
		end
		--write out result strings
		for _, comp in pairs(comp_str) do print(comp) end
	end
end

function bom.masterProcessBom(filename)
	--Get file data
	local f = assert(io.open(filename, "r"))
	local boards = loadstring(bom:processBadGreek(f:read("*a")))()
	f:close()
	f = nil 
	
	--condense the library
	local lib = bom:condense(boards)
	
	lib.resistor = bom:sortByHeirarchy(lib.resistor, dofile("kicad_org\\sort_test.lua"))

	bom.present(lib)
end

return bom