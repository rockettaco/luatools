local mill={}

mill.precision = 3 --Decimal places in printed numbers

--All units are defined relative to millimeter
mill.units={
	inch = {
		abbrev = "in",
		factor = 25.4,
	},
	mm = {
		abbrev = "mm",
		factor = 1,
	},
}

--Enum of types of actions in a sequence (whoops, I just reinvented G-code)
mill.ActionType={
	move    = {}, --Move the mill to a location without cutting
	cut     = {}, --Cut to a location
	comment = {}, --Print an associated comment string
	stage   = {}, --Signal moving to next stage (print a group break)
}

--Enumerator for axes
mill.axes={x=1, y=2, z=3}
local axes = mill.axes --Save a lot of typing, also maybe faster?
--Display names and other axis-specific data
mill.axis_data={
	[axes.x]={
		name="X",
		cut_str="CUT",
	}, 
	[axes.y]={
		name="Y",
		cut_str="CUT",
	},
	[axes.z]={
		name="Z",
		cut_str="PLUNGE",
	},
}

local function addVec(a,b) return {a[1] + b[1], a[2] + b[2], a[3] + b[3]} end
local function subVec(p,n) return {p[1] - n[1], p[2] - n[2], p[3] - n[3]} end
local function copyVec(dest, src) for i,v in ipairs(src) do dest[i] = v end return end

mill.sequence = {} --Functionality for recording a motion sequence
--Generate a new sequence from scratch
function mill.sequence.new()
	return {
		steps = {},
		origin = {0, 0, 0},
		reference = "",
	}
end

--Add an action to a sequence
function mill.sequence.addAct(seq, act_type, action, axis, relative)
	local new_step = {
		act_type = act_type,
		axis = axis,
		value = action,
		relative = relative
	}
	table.insert(seq.steps, new_step)
end

--Set the origin on one axis of a sequence
function mill.sequence.setOrigin(seq, axis, value)
	seq.origin[axis] = value
end

--[[
	Get a number as a string formatted to a specific unit with precision given
	by the global mill.precision
	Arguments:
		value   Number to be formatted, in native units
		unit    Display units, from mill.units
--]]
local function unitStr(value, unit)
	return string.format("%." .. tostring(mill.precision) .. "f %s", value / unit.factor, unit.abbrev)
end

--[[
	Get instructions for milling moves or cuts. If turn_dist is given, this will
	give the number of times the dial should pass zero and the number to stop at
	once it has. Otherwise, it will give the destination point as an absolute
	position. Note that "turns" is NOT complete revolutions, but turns past zero.
	All <Vec> arguments are points in the form of arrayed 3-vectors of numbers,
	and should be in native units.
	Arguments:
		unit       Display units, from mill.units
		prev_pos   Move starting position, in native units
		new_pos    Move stopping position, in native units
		origin     Global offset applied to all displayed values
		backlash   Slop in machine travel
		turn_dist  Native units per revolution of handwheel
-]]
function mill.renderMove(unit, prev_pos, new_pos, origin, backlash, turn_dist)
	--Decide whether to add backlash - only in +n direction since we zero in -n
	if new_pos > prev_pos then new_pos = new_pos + backlash end
	if nil == turn_dist then
		return unitStr(new_pos - origin, unit)
	else
		local turns = math.floor((new_pos - origin) / turn_dist) - math.floor((prev_pos - origin) / turn_dist)
		return string.format("%d turns to %s", turns, unitStr((new_pos - origin) % turn_dist, unit))
	end
end

--[[
function mill.getMoveStr(last_pos, new_pos, origin, unit, turn_dist)
	if nil == turn_dist then
		return unitStr(new_pos - origin, unit)
	else
		local turns = math.floor((new_pos - origin) / turn_dist) - math.floor((last_pos - origin) / turn_dist)
		if false then turns = turns - 1 end
		return string.format("%2d turns to %s", turns, unitStr((new_pos - origin) % turn_dist, unit))
	end
end
--]]

--[[
	sequence={
		steps[n]={         Array of actions that make up the sequence
			act_type       Action type ID code, from mill.actionType
			axis           Axis (or nil) the action works upon
			value          Data (or nil) associated with the action
		}
		origin={x,y,z}     Location of the origin relative to reference point
		reference          String describing reference point from which origin is offset
	}
	unit                   Display units, from mill.units
	opts={                 Control of output and machine characteristics, all optional
		turn_dist[xyz]={   If absent output for given axis will be as absolutes, if present will be turns + stop point
			value          Distance per turn of handwheels
			unit           Units for distance per turn, from mill.units
		}
		backlash[xyz]={    Slop in machine for given axis - will be zero if not present
				value      Slop motion distance
				unit       Units for slop, from mill.units
		}
		origin_offs[xyz]={ Offset applied to all coordinates displayed
				value      Offset for this axis
				unit       Units for offset, from mill.units
		}
--]]
function mill.render(sequence, unit, opts)
	local result = {}

	--Deconstruct option arguments and sort out units
	local origin = addVec(sequence.origin, opts.origin_offset or {[axes.x]=0, [axes.y]=0, [axes.z]=0})
	local backlash
	if nil ~= opts.backlash then
		backlash = {
			[axes.x] = (opts.backlash[axes.x] ~= nil) and opts.backlash[axes.x].value * opts.backlash[axes.x].unit.factor or 0,
			[axes.y] = (opts.backlash[axes.y] ~= nil) and opts.backlash[axes.y].value * opts.backlash[axes.y].unit.factor or 0,
			[axes.z] = (opts.backlash[axes.z] ~= nil) and opts.backlash[axes.z].value * opts.backlash[axes.z].unit.factor or 0,
		}
	else
		backlash = {[axes.x]=0, [axes.y]=0, [axes.z]=0}
	end
	local turn_dist
	if nil ~= opts.turn_dist then
		turn_dist = {
			[axes.x] = (opts.turn_dist[axes.x] ~= nil) and opts.turn_dist[axes.x].value * opts.turn_dist[axes.x].unit.factor or nil,
			[axes.y] = (opts.turn_dist[axes.y] ~= nil) and opts.turn_dist[axes.y].value * opts.turn_dist[axes.y].unit.factor or nil,
			[axes.z] = (opts.turn_dist[axes.z] ~= nil) and opts.turn_dist[axes.z].value * opts.turn_dist[axes.z].unit.factor or nil,
		}
	else
		turn_dist = {}
	end
	local last_pos = {}
	copyVec(last_pos, origin)

	--Process the steps
	local act = mill.ActionType --Clearer code but also faster
	local start_line = true
	local new_pos
	--TODO: origin
	for _,step in ipairs(sequence.steps) do
		if step.act_type == act.cut then
			if not start_line then table.insert(result, " | ") end
			start_line = false
			table.insert(result, mill.axis_data[step.axis].cut_str)
			table.insert(result, " ")
			table.insert(result, mill.axis_data[step.axis].name)
			table.insert(result, "-> ")
			if(step.relative) then
				new_pos = last_pos[step.axis] + step.value
			else
				new_pos = step.value
			end
			table.insert(result, mill.renderMove(unit, last_pos[step.axis], new_pos, origin[step.axis], backlash[step.axis], turn_dist[step.axis]))
			last_pos[step.axis] = new_pos
		elseif step.act_type == act.move then
			if not start_line then table.insert(result, " | ") end
			start_line = false
			table.insert(result, "MOVE ")
			table.insert(result, mill.axis_data[step.axis].name)
			table.insert(result, "-> ")
			if(step.relative) then
				new_pos = last_pos[step.axis] + step.value
			else
				new_pos = step.value
			end
			table.insert(result, mill.renderMove(unit, last_pos[step.axis], new_pos, origin[step.axis], backlash[step.axis], turn_dist[step.axis]))
			last_pos[step.axis] = new_pos
		elseif step.act_type == act.stage then
			if not start_line then table.insert(result, "\n") end
			start_line = true
		elseif step.act_type == act.comment then
			if not start_line then table.insert(result, "\n") end
			table.insert(result, tostring(step.value))
			table.insert(result, "\n")
			start_line = true
		else
			perror("Unknown action type")
		end
	end
	return table.concat(result)
end

--[[ Tool to give instructions for cutting slots with curved depth profile.
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement, from mill.units
		}
		profiles={
			n*{           Array of measurements of slot length vs depth
				depth     Depth of slot at measurement
				length    Length of slot at this depth
			}
			unit          Unit used for profile measurements, from mill.units
		}
--]]
function mill.slot(tool, profiles)
	local result = mill.sequence.new()
	local act = mill.ActionType --Clearer code but also faster
	local axes = mill.axes --See above
	--Get native units
	tool = tool.width * tool.unit.factor
	result.reference = "center of slot"
	mill.sequence.setOrigin(result, axes.x, 0 - ((profiles[1].length * profiles.unit.factor) - tool) / 2)
	local reverse_pass = true --direction of movement is toward origin point
	local target
	for i = 1, #profiles do
		target = ((profiles[i].length * profiles.unit.factor) - tool) / 2
		if i ~= 1 then
			mill.sequence.addAct(result, act.move, reverse_pass and 0 - target or target, axes.x, false)
			mill.sequence.addAct(result, act.stage)
		end
		mill.sequence.addAct(result, act.cut, profiles[i].depth * profiles.unit.factor, axes.z, false)
		mill.sequence.addAct(result, act.cut, reverse_pass and target or 0 - target, axes.x, false)
		reverse_pass = not reverse_pass
	end
	return result
end

--[[ Tool to give instructions for milling a rectangle from the outside
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement, from mill.units
		}
		rect={
			x             X-axis size of rectangle
			y             Y-axis size of rectangle
			unit          Units of rectangle measurements, from mill.units
		}
		opt={
			stepover      Percentage of tool width to cut on each pass, as a scalar
			excess={
				value     Width of excess stock around desired profile (start distance)
				unit      Units of excess stock width, from mill.units
			}
		}
--]]
function mill.rectOutside(tool, unit, input_rect, opt)
	local result = mill.sequence.new()
	local act = mill.ActionType --Clearer code but also faster
	local axes = mill.axes --See above
	--Get native units, set default values
	tool = tool.width * tool.unit.factor
	result.reference = "center of square"
	local stepover = (opt.stepover ~= nil) and opt.stepover * tool or tool
	local excess = (opt.excess ~= nil) and opt.excess.value * opt.excess.unit.factor or 0
	local rect = {
		x = input_rect.x * input_rect.unit.factor,
		y = input_rect.y * input_rect.unit.factor,
	}
	mill.sequence.setOrigin(result, axes.x, 0 - ((rect.x + tool) / 2) - excess)
	mill.sequence.setOrigin(result, axes.y, 0 - ((rect.y + tool) / 2) - excess)

	--Cut the rectangle
	local first_pass = true
	repeat
		excess = excess > stepover and excess - stepover or 0
		if first_pass then
			mill.sequence.addAct(result, act.move, stepover, axes.y, true)
			first_pass = false
		else
			mill.sequence.addAct(result, act.cut, 0 - (((rect.y + tool) / 2) + excess), axes.y, false)
		end
		mill.sequence.addAct(result, act.stage)
		mill.sequence.addAct(result, act.cut, ((rect.x + tool) / 2) + excess, axes.x, false)
		mill.sequence.addAct(result, act.cut, rect.y + tool + (2 * excess), axes.y, true)
		mill.sequence.addAct(result, act.cut, 0 - (rect.x + tool + (2 * excess)), axes.x, true)
	until excess == 0
	mill.sequence.addAct(result, act.cut, 0 - (rect.y + tool), axes.y, true)
	return result
end

return mill
