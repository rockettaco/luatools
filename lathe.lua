local lathe = {}

lathe.changeGears = {
	20, 20, 21, 35, 40, 45, 50, 55, 57, 60, 65, 80, 80,
}

function lathe.pitchFromChangeGears(leadscrew, gears)
	return (leadscrew * gears[1] * gears[3]) / (gears[2] * gears[4])
end

function lathe.changeGearError(leadscrew, target_pitch, gears)
	local true_pitch = lathe.pitchFromChangeGears(leadscrew, gears)
	local err = {}
	err.factor = true_pitch / target_pitch
	err.percent = err.factor - 1
	err.ten_pitch = 10 * (target_pitch - (err.factor * target_pitch))
	err.percent = err.percent * 100
	return err
end

--does NOT account for actually being able to fit the gears
function lathe.nearestChangeGear(leadscrew, target_pitch, gear_set)
	local best
	local results = {}
	local err
	for a = 1, #gear_set do
		for b = 1, #gear_set do
			if b ~= a then for c = 1, #gear_set do
				if (c ~= a) and (c ~= d) then for d = 1, #gear_set do
					if (d ~= a) and (d ~= b) and (d ~= c) then
						err = lathe.changeGearError(leadscrew,
						                            target_pitch,
													{gear_set[a], gear_set[b], gear_set[c], gear_set[d]}
						                           ).percent
						if (nil == best) or (math.abs(err) < best) then
							best = math.abs(err)
							results = {}
						end
						if (math.abs(err) <= best) then
							table.insert(results, {gear_set[a], gear_set[b], gear_set[c], gear_set[d]})
						end
					end
				end end
			end end
		end
	end
	return results
end

function lathe.gearForces(gears)
	local result = {}
	result.ratio_motor = gears[1] / gears[2]
	result.ratio_leadscrew = gears[3] / gears[4]
	result.force_leadscrew = 1 / gears[4]
	result.force_motor = result.ratio_leadscrew / gears[2]
	return result
end

return lathe
