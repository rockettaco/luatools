local xml = {}

--Functions used in parsing XML files
xml.parser = {}

--Replace XML escape sequences with their equivalents
function xml.parser.subEscapes(str)
	str = string.gsub(str, "&lt;", "<")
	str = string.gsub(str, "&amp;", "&")
	str = string.gsub(str, "&gt;", ">")
	str = string.gsub(str, "&quot;", "\"")
	str = string.gsub(str, "&quot;", "'")
	return str
end

--[[
	The subsection parsing functions each take three arguments:
		1.	The source string
		2.	The starting index of the subsection
		3.	The stack of currently open element tables
	They return 1 value:
		1.	The index of the first character in the	source string following the subsection
	These functions must only be invoked on strings known to start with their subsection start
	tokens. Failure to comply may produce misleading error messages.

	At the present time many of these regexes are anchored to the beginning of the string to help
	catch calling dispatch errors. This may or may not have performance effects in use.
--]]

--Interpret an attribute list (x="y" ...)
--Note this will return the index of the first character after the last recognized attribute in this
--list; this function does NOT consume the markup that ends the tag.
function xml.parser.parseAttribs(source, start, stack)
	local cap_start, cap_end, new_name, data, end_char
	while true do
		--Get the next name-value pair
		cap_start, cap_end, new_name, _, value = string.find(source, "^%s*([%a_:][%w%.%-_:]-)%s*=%s*([\"'])(.-)%2", start)
		if nil == cap_start then break end
		--Add to element's table created by calling function
		stack[#stack].attributes = stack[#stack].attributes or {}
		table.insert(stack[#stack].attributes, {name=new_name, xml.parser.subEscapes(value)})
		start = cap_end + 1
		if not "" == end_char then break end
	end
	return start
end

--Interpret an opening tag (<... >)
function xml.parser.parseOpening(source, start, stack)
	local cap_start, cap_end, end_char, new_name, new_table
	--TODO: check legal name characters
	cap_start, cap_end, new_name = string.find(source, "^<([%a_][%w%-%._]+)", start)
	if nil == cap_start then
		error("Opening tag at character #" .. start .. " does not begin with valid name")
	end
	--Create a new element with this name, parse any attributes it may have
	new_table = {name=new_name}
	table.insert(stack[#stack], new_table)
	table.insert(stack, new_table)
	start = xml.parser.parseAttribs(source, cap_end + 1, stack)
	--Consume the termination of the tag
	cap_start, cap_end, end_char = string.find(source, "^%s-(/?)>", start)
	if nil == cap_start then
		error("Expected end of tag after character #" .. start)
	end
	--Self-closing tags
	if "" ~= end_char then
		table.remove(stack)
	end
	return cap_end + 1
end

--Interpret a closing tag (</...>)
function xml.parser.parseClosing(source, start, stack)
	local cap_start, cap_end, name
	--TODO: check legal name characters
	cap_start, cap_end, name = string.find(source, "^</([%a_][%w%-%._]+)%s->", start)
	if nil == cap_start then
		error("Improper closing tag at character #" .. start)
	end
	if not stack[#stack].name == name then
		error("Attempted to close element \"" .. stack[#stack].name .. "\" as element \"" .. name .. "\"")
	end
	table.remove(stack)
	return cap_end + 1
end

--Interpret an XML processing instruction (<? ... ?>
function xml.parser.parseProcInstr(source, start, stack)
	local cap_start, cap_end, proc_instr
	cap_start, cap_end, proc_instr = string.find(source, "^<%?(.-)%?>", start)
	if nil == cap_start then
		error("Unterminated processing instruction at character #" .. start)
	end
	--TODO: expanded parsing for common variants
	stack.meta.proc_instr = stack.meta.proc_instr or {}
	table.insert(stack.meta.proc_instr, proc_instr)
	return cap_end + 1
end

--Interpret a directive (<! ... >
function xml.parser.parseDirective(source, start, stack)
	--Comment
	if nil ~= string.match(source, "^<!--", start) then
		return xml.parser.parseComment(source, start, parent)
	end
	--CDATA section
	if nil ~= string.match(source, "^<!%[CDATA%[", start) then
		return xml.parser.parseCData(source, start, parent)
	end
	--DOCTYPE declaration
	if nil ~= string.match(source, "<!DOCTYPE", start) then
		return xml.parser.parseDoctype(source, start, parent)
	end
	error("Unrecognized directive: " .. string.match(source, "<!([^%s]-)") .. " at character #" .. start)
end

--Interpret (skip over) a comment (<!-- ... -->)
function xml.parser.parseComment(source, start, stack)
	local cap_start, cap_end
	cap_start, cap_end = string.find(source, "-->", start)
	if nil == cap_start then
		error("Unterminated comment at character #" .. start)
	end
	return cap_end + 1
end

--Interpret a CDATA section (<![CDATA[ ... ]]>)
function xml.parser.parseCData(source, start, stack)
	local cap_start, cap_end, cdata
	cap_start, cap_end, cdata = string.find(source, "<!%[CDATA%[(.-)%]%]>", start)
	if nil == cap_start then
		error("Unterminated CDATA section at character #" .. start)
	end
	table.insert(stack[#stack], cdata)
	return cap_end + 1
end

--Interpret a DOCTYPE section (<!DOCTYPE ... >
function xml.parser.parseDoctype(source, start, stack)
	local cap_start, cap_end, doctype_data
	cap_start, cap_end, doctype_data = string.find(source, "<!DOCTYPE%s+(.-)%s->", start)
	if nil == cap_start then
		error("Unterminated DOCTYPE section at character #" .. start)
	end
	--TODO: expanded parsing for specific DOCTYPE instructions
	stack.meta.doctype = stack.meta.doctype or {}
	table.insert(stack.meta.doctype, doctype_data)
	return cap_end + 1
end

--Parse an XML document to an object/table tree; this is the master function invoked by user code
function xml.parse(source)
	local stack = { meta={}, {} }
	local cur_index, next_index, start_char, text
	cur_index = 1
	while true do
		--Find next tag start and capture any text preceding
		--print("polling at #" .. cur_index)
		next_index, _, start_char = string.find(source, "<([%/%?!]?)", cur_index)
		--print("Result at: " .. (next_index or "nil") .. "\nText: " .. (text or "nil"))
		if not next_index then break end
		--Add any non-element text in order
		text = string.sub(source, cur_index, next_index - 1)
		if string.find(text, "[^%s]") then
			if #stack == 1 then
				error("Text outside root element")
			end
			table.insert(stack[#stack], xml.parser.subEscapes(text))
		end
		
		--Dispatch on start of tag
		--Opening tag
		if     "" == start_char then
			cur_index = xml.parser.parseOpening(source, next_index, stack)
		--Closing tag
		elseif "/" == start_char then
			cur_index = xml.parser.parseClosing(source, next_index, stack)
		--XML processing instruction
		elseif "?" == start_char then
			cur_index = xml.parser.parseProcInstr(source, next_index, stack)
		--XML comment or directive
		elseif "!" == start_char then
			cur_index = xml.parser.parseDirective(source, next_index, stack)
		--Some other start char we don't know
		else
			error("Unrecognized tag opening following character #" .. start_char)
		end
	end
	--check for unclosed elements
	if #stack > 1 then
		error ("Unclosed element \"" .. stack[#stack].name .. "\"")
	end
	--check that unprocessed characters are all whitespace
	if string.find(source, "[^%s]", cur_index) then
		error("Uncontained text at end of document")
	end
	--check for multiple root elements
	if #stack[1] > 1 then
		error("Multiple root elements")
	end
	return {meta=stack.meta, stack[1][1]}
end

--Functions for extracting programmatic table views from parsed XML element structures
xml.extract = {}

--Find an element or attribute entry with the given name
function xml.extract.findEntry(parent, name)
	for i,v in ipairs(parent) do
		if v.name == name then return v end
	end
end

--Extract an appropriate native representation for a value as determined by options
local function extractVal(data, text_bool, num_bool, num)
	if text_bool then
		local lower = string.lower(data)
		if     lower == "true" then return true
		elseif lower == "false" then return false
		end
	end
	local num_rep num_rep = tonumber(data)
	if num_bool and #data == 1 then
		if     0 == num_rep then return true
		elseif 1 == num_rep then return false
		end
	end
	if num and num_rep then return num_rep
	else return data
	end
end

--[[
Extract table view from an XML document
valid options:
		merge_attribs		map attributes as sub-elements; else they map to [0]
		extract_text_bool	map "true" and "false" as booleans
		extract_num_bool	map "0" and "1" as booleans
		extract_numbers	map numeric values as numbers, unless overridden by extract_num_bool
--]]
function xml.extract.generic(data, options)
	options = options or {}
	local result = {}
	--create collections of elements with each name
	for i,v in ipairs(data) do
		if "table" == type(v) then
			result[v.name] = result[v.name] or {}
			table.insert(result[v.name], xml.extract.generic(v, options))
		else
			table.insert(result, extractVal(v, options.extract_text_bool, options.extract_num_bool, options.extract_numbers))
		end 
	end
	--either merge the attributes, or put them in [0]
	if nil ~= data.attributes then
		if options.merge_attribs then
			for i,v in ipairs(data.attributes) do
				result[v.name] = result[v.name] or {}
				table.insert(result[v.name], extractVal(v[1], options.extract_text_bool, options.extract_num_bool, options.extract_numbers))
			end
		else
			for i,v in ipairs(data.attributes) do
				result[0] = result[0] or {}
				result[0][v.name] = extractVal(v[1], options.extract_text_bool, options.extract_num_bool, options.extract_numbers)
			end
		end
	end
	--collapse collections with only one element (unique subelements)
	for k,v in pairs(result) do
		if "table" == type(v) and #v == 1 then result[k] = v[1] end --should not affect attribute [0] tables, they have no numeric indices
	end
	--return anything with ONLY text data as that text data
	if #result == 1 then return result[1] else return result end
	return result
end

--Extract table view from a parsed file of format:
--	ARM device System View Description (SVD)
--	extensions: .svd, .xml
function xml.extract.svd(data)
	local result = xml.extract.generic(data, {extract_text_bool=true, extract_numbers=true})
	for _,p in ipairs(result.device.peripherals.peripheral) do
		result.device.peripherals[p.name] = p
		p.name = nil
		if nil ~= p.registers then
			for _,r in ipairs(p.registers.register) do
				p.registers[r.name] = r
				r.name = nil
				if nil ~= r.fields then
					for _,f in ipairs(r.fields.field) do
						r.fields[f.name] = f
						f.name = nil
						if nil ~= f.enumeratedValues then
							for _,v in ipairs(f.enumeratedValues.enumeratedValue) do
								f.enumeratedValues[v.name] = v
								v.name = nil
							end
							f.enumeratedValues.enumeratedValue = nil
						end
					end
					r.fields.field = nil
				end
			end
			p.registers.register = nil
		end
		--handle cross references
		if p[0] then
			if p[0].derivedFrom then
				p.derivedFrom = result.device.peripherals[p[0].derivedFrom]
			end
			p[0] = nil
		end
	end
	result.device.peripherals.peripheral = nil
	return result
end

--Functions for operating on and inspecting XML data
xml.util = {}

--Print the contents of an element table in a human-readable manner. Not robust.
function xml.util.browse(table)
	local value
	print("Elements:")
	for i,v in ipairs(table) do
		value = v.name or '"' .. v .. '"'
		print("\t" .. tostring(i) .. " = " .. tostring(value))
	end
	if nil ~= table.attributes then
		print("Attributes:")
		for i,v in ipairs(table.attributes) do
			print("\t" .. tostring(i) .. " = " .. tostring(v.name))
		end
	end
end

return xml