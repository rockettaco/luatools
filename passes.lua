return {
	--Control
	{speed=6000, feed=800, depth=1.0},
	{speed=6000, feed=800, depth=1.0},

	--Depth variance
	{speed=6000, feed=800, depth=0.5},
	{speed=6000, feed=800, depth=0.5},
	{speed=6000, feed=800, depth=1.5},
	{speed=6000, feed=800, depth=1.5},

	--Feed/chipload variance
	{speed=6000, feed=400, depth=1.0},
	{speed=6000, feed=400, depth=1.0},
	{speed=6000, feed=1200, depth=1.0},
	{speed=6000, feed=1200, depth=1.0},

	--Speed variance
	{speed=8000, feed=1066, depth=1.0},
	{speed=8000, feed=1066, depth=1.0},
	{speed=4000, feed=533, depth=1.0},
	{speed=4000, feed=533, depth=1.0},

}
