local GCode = {}

function GCode:moveLinear(pos, rapid)
	table.insert(self.sequence, rapid and "G00" or "G01")
	if pos.x then table.insert(self.sequence, " X" .. tostring(pos.x) end
	if pos.y then table.insert(self.sequence, " Y" .. tostring(pos.y) end
	if pos.z then table.insert(self.sequence, " Z" .. tostring(pos.z) end
	table.insert(self.sequence, "\n")
end

function GCode:compile()
	table.concat(self.sequence)
end

return GCode
