local mill={}

--All units are defined relative to millimeter
mill.units={
	inch = {
		abbrev = "in",
		factor = 25.4,
	},
	mm = {
		abbrev = "mm",
		factor = 1,
	},
}

mill.actionType={
	move    = {}, --Move the mill to a location without cutting
	cut     = {}, --Cut to a location
	comment = {}, --Print an associated comment string
	stage   = {}, --Signal moving to next stage (print a group break)
}

mill.axis={x=1, y=2, z=3}
mill.axisNames={[1]="X", [2]="Y", [3]="Z"}

mill.precision = 3      --Decimal places in printed numbers

mill.sequence = {} --Functionality for recording a motion sequence
function mill.sequence.new()
	return {
		steps = {},
		origin = {0, 0, 0},
		reference = "",
	}
end

function mill.sequence.addAct(seq, act_type, axis, action)
	local new_step = {
		act_type = act_type,
		axis = axis,
		value = action,
	}
	table.insert(seq.steps, new_step)
end

function mill.sequence.setOrigin(seq, axis, value)
	seq.origin[axis] = value
end

--[[
	Get a number as a string formatted to a specific unit with precision given
	by the global mill.precision
	Arguments:
		value   Number to be formatted, in native units
		unit    Display units, from mill.units
--]]
local function unitStr(value, unit)
	return string.format("%." .. tostring(mill.precision) .. "f %s", value / unit.factor, unit.abbrev)
end

--[[
	Get instructions for milling moves or cuts. If turn_dist is given, this will
	give the number of times the dial should pass zero and the number to stop at
	once it has. Otherwise, it will give the destination point as an absolute
	position. Note that "turns" is NOT complete revolutions, but turns past zero.
	Arguments:
		last_pos   Move starting position, in native units
		new_pos    Move stopping position, in native units
		turn_dist  Native units per revolution of handwheel
		unit       Display units, from mill.units
-]]
function mill.getMoveStr(last_pos, new_pos, origin, unit, turn_dist)
	if nil == turn_dist then
		return unitStr(new_pos - origin, unit)
	else
		local turns = math.floor((new_pos - origin) / turn_dist) - math.floor((last_pos - origin) / turn_dist)
		if false then turns = turns - 1 end
		return string.format("%2d turns to %s", turns, unitStr((new_pos - origin) % turn_dist, unit))
	end
end

--[[
	sequence={
		steps={
			act_type=mill.actionType.steps
			axis = ...
			value = ...
		}
		origin={   Location of the origin relative to reference point
			x
			y
			z
		}
		reference  String describing reference point from which origin is offset
	}
	unit           Display units, from mill.units
	turn_dist={    OPTIONAL, if absent output will be as absolutes, if present will be turns + stop point
		value      Distance per turn of handwheels
		unit       Units for distance per turn, from mill.units
	}
--]]
function mill.render(sequence, unit, turn_dist)
	local result = {}
	if not (turn_dist == nil) then turn_dist = turn_dist.value * turn_dist.unit.factor end
	local act = mill.actionType --Clearer code but also faster
	local axes = mill.axisNames --See above
	local start_line = true
	local last_pos = {sequence.origin[1], sequence.origin[2], sequence.origin[3]}
	for _,step in ipairs(sequence.steps) do
		if step.act_type == act.cut then
			if not start_line then table.insert(result, " | ") end
			table.insert(result, "CUT ")
			table.insert(result, axes[step.axis])
			table.insert(result, "-> ")
			table.insert(result, mill.getMoveStr(last_pos[step.axis], step.value, sequence.origin[step.axis], unit, turn_dist))
			start_line = false
			last_pos[step.axis] = step.value
		elseif step.act_type == act.move then
			if not start_line then table.insert(result, " | ") end
			table.insert(result, "MOVE ")
			table.insert(result, axes[step.axis])
			table.insert(result, "-> ")
			table.insert(result, mill.getMoveStr(last_pos[step.axis], step.value, sequence.origin[step.axis], unit, turn_dist))
			start_line = false
			last_pos[step.axis] = step.value
		elseif step.act_type == act.stage then
			if not start_line then table.insert(result, "\n") end
			start_line = true
		elseif step.act_type == act.comment then
			if not start_line then table.insert(result, "\n") end
			table.insert(result, tostring(step.value))
			table.insert(result, "\n")
			start_line = true
		else
			--perror("Unknown action type")
		end
	end
	return table.concat(result)
end

--[[ Tool to give instructions for cutting slots with curved depth profile.
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement, from mill.units
		}
		profiles={
			n*{           Array of measurements of slot length vs depth
				depth     Depth of slot at measurement
				length    Length of slot at this depth
			}
			unit          Unit used for profile measurements, from mill.units
		}
		opt={
			backlash={
				value    Slop present in slot long axis on machine
				unit     Units for slop, from mill.units
			}
--]]
function mill.slot(tool, profiles, opt)
	local result = mill.sequence.new()
	local act = mill.actionType --Clearer code but also faster
	local axes = mill.axis --See above
	--Get native units, set backlash default value
	local backlash = (opt.backlash ~= nil) and opt.backlash.value * opt.backlash.unit.factor or 0
	tool = tool.width * tool.unit.factor
	result.reference = "center of slot"
	mill.sequence.setOrigin(result, axes.x, 0 - ((profiles[1].length * profiles.unit.factor) - tool) / 2)
	local reverse_pass = true --direction of movement is toward origin point
	local target
	for i = 1, #profiles do
		target = ((profiles[i].length * profiles.unit.factor) - tool) / 2
		if i ~= 1 then
			mill.sequence.addAct(result, act.move, axes.x, reverse_pass and target + backlash or target)
			mill.sequence.addAct(result, act.stage, nil, nil)
		end
		mill.sequence.addAct(result, act.cut, axes.z, profiles[i].depth * profiles.unit.factor)
		mill.sequence.addAct(result, act.cut, axes.x, reverse_pass and target + backlash or 0 - target)
		reverse_pass = not reverse_pass
	end
	return result
end

--[[ Tool to give instructions for milling a rectangle from the outside
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement, from mill.units
		} 
		unit              Display units for output, from mill.units
		rect={
			x             X-axis size of rectangle
			y             Y-axis size of rectangle
			unit          Units of rectangle measurements, from mill.units
		}
		opts={
			stepover      Percentage of tool width to cut on each pass, as a scalar
			excess={
				value     Width of excess stock around desired profile (start distance)
				unit      Units of excess stock width, from mill.units
			}
			backlash_x={
				value     Slop present in X axis on machine
				unit      Units of X axis slop, from mill.units
			}
			backlash_y={
				value     Slop present in Y axis on machine
				unit      Units of Y axis slop, from mill.units
			}
		}
--]]
function mill.rectOutsideAbsolute(tool, unit, input_rect, opt)
	--Get native units, set default values
	tool = tool.width * tool.unit.factor
	local stepover = (opt.stepover ~= nil) and opt.stepover * tool or tool
	local excess = (opt.excess ~= nil) and opt.excess.value * opt.excess.unit.factor or 0
	local backlash_x = (opt.backlash_x ~= nil) and opt.backlash_x.value * opt.backlash_x.unit.factor or 0
	local backlash_y = (opt.backlash_y ~= nil) and opt.backlash_y.value * opt.backlash_y.unit.factor or 0
	local rect = {
		x = input_rect.x * input_rect.unit.factor,
		y = input_rect.y * input_rect.unit.factor,
	}
	--[[ Set the origin at toolpoint for start of cut. We will internally
	     calculate with the origin as the center of the rectangle for simplicity,
         but the output will be written as relative to this point to ease using
	     the dials/DRO.
	--]]
	local origin_x = 0 - ((rect.x + tool) / 2) - excess
	local origin_y = 0 - ((rect.y + tool) / 2) - excess
	print(string.format("Set origin to surface, X=%s, Y=%s from center - approach both axes from center to align backlash", unitStr(origin_x, unit), unitStr(origin_y, unit)))
	local x_target
	local y_target
	repeat
		--Get the next cut height
		excess = excess > stepover and excess - stepover or 0
		--Set the tool center distances
		target_x = ((rect.x + tool) / 2) + excess
		target_y = ((rect.y + tool) / 2) + excess
		io.write("Set Y=" .. unitStr(0 - target_y + backlash_y - origin_y, unit))
		io.write(" | Cut X=" .. unitStr(target_x + backlash_x - origin_x, unit))
		io.write(" | Cut Y=" .. unitStr(target_y + backlash_y - origin_y, unit))
		io.write(" | Cut X=" .. unitStr(0 - target_x - origin_x, unit))
		io.write(" | Cut Y=" .. unitStr(0 - target_y - origin_y, unit))
		io.write("\n")
	until excess == 0
	print("Cut through corner and done")
end

return mill
