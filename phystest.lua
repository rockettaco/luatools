local physlib = {}


--[[physlib.Scalar = {
	meta = {
		__add = 
		__sub = 
		__mul = 
		__mod = 
		__pow = 
		__unm = 
		__eq = 
		__lt = 
		__le = 
		__index = 
		__newindex = 
		__call = 
	}
}

physlib.addMatrix
local meta_vector = {
	__add = function(op1, op2)
		
	end
}--]]

physlib.Vector = {
	meta = {
		__add = function(op1, op2)
			if #(op1.value) ~= #(op2.value) then
				error("Cannot add vectors of different lengths")
			end
			
			local result = physlib.Vector.new()
			result.value = {}
			for i,v in ipairs(op1.value) do
				result.value[i] = v + (op2.value[i] or 0)
			end
			return result
		end,
	},

	new = function(...)
		local newVector = {}
		newVector.value = {...}
		setmetatable(newVector, physlib.Vector.meta)
		return newVector
	end,
	
	distance = function(self)
		local square_sum = 0
		for _,v in ipairs(self.value) do
			square_sum = square_sum + (v^2)
		end
		return math.sqrt(square_sum)
	end
}

physlib.Vector.meta.__index = physlib.Vector
return physlib