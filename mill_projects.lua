mill = require("mill")
util = require("util")

mill_projects={}

mill_projects.tools = {
	maillefer_bur_1mm = {
		width = 1,
		unit  = mill.units.mm,
	},
	maillefer_bur_2p1mm = {
		width = 2.1,
		unit  = mill.units.mm,
	},
	mill_1f4in = {
		width = 1/4,
		unit  = mill.units.inch,
	},
	mill_3f8in = {
		width = 3/8,
		unit  = mill.units.inch,
	},
	mill_1f2in = {
		width = 1/2,
		unit  = mill.units.inch,
	},
}

mill_projects.cold_plate = {
	slot={
		{depth=0.2, length=29.3},
		{depth=0.4, length=28.5},
		{depth=0.6, length=27.7},
		{depth=0.8, length=26.8},
		{depth=1.0, length=25.9},
		{depth=1.2, length=24.8},
		{depth=1.4, length=23.6},
		{depth=1.6, length=22.2},
		{depth=1.8, length=20.4},
		{depth=2.0, length=17.9},
		{depth=2.1, length=14.3},
		unit = mill.units.mm,
	},
	base={
		x    = 56,
		y    = 49,
		unit = mill.units.mm,
	},
	contact={
		x    = 36,
		y    = 28,
		unit = mill.units.mm,
	},

	showAll = function()
		print("======================")
		print("MILLING PROCEDURES FOR COLD PLATE")
		print("======================")
		local slots = mill_projects.cold_plate.slot
		local base = mill_projects.cold_plate.base
		local contact = mill_projects.cold_plate.contact

		local tool = mill_projects.tools.maillefer_bur_1mm
		local unit = mill.units.mm
		local opt = {
			backlash = {
				[mill.axes.x]={
					value = .03,
					unit  = mill.units.inch
				},
				[mill.axes.y]={
					value = .028,
					unit  = mill.units.inch,
				},
			},
			--[[
			turn_dist = {
				[mill.axes.x]={
					value = 0.1,
					unit = mill.units.inch,
				},
				[mill.axes.y]={
					value = 0.1,
					unit = mill.units.inch,
				},
			},
			--]]
			excess = {
				value = 3,
				unit = mill.units.mm,
			},
			stepover = 0.2,
		}

		print("SLOTTING:")
		print(mill.render(mill.slot(tool, slots, opt), unit, opt))
		print("======================")

		tool = mill_projects.tools.mill_1f2in
		print("MILLING OUTER EDGE:")
		print("Set Z to //TODO:")
		print(mill.render(mill.rectOutside(tool, mill.units.inch, base, opt), unit, opt))
		print("======================")

		opt.excess.value = 12
		print("MILLING CONTACT SURFACE")
		print("Set Z to //TODO:")
		print(mill.render(mill.rectOutside(tool, mill.units.inch, contact, opt), unit, opt))
		print("======================")
	end,
	--OAH 4.55, stock 4.76, 0.1 surfacing
}

return mill_projects
