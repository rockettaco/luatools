local comm = {}

--[[ structure of a communications handler
comm.internal = {
	port = {
		send()     --Function to push data onto the outgoing bus
		receive()  --Function to retrieve data (if any) from the incoming bus
	},                 
	messages = {}, --Array for storing message data
	buffer = "",   --Raw byte stream of received data
	packetize(),   --Function to identify packets in stream and separate them. Takes <buffer> as an
	               --argument, and returns length of packet starting at [1] (IOW, index of last byte
	               --in packet) followed by message sequence ID number, then an arbitrary data field
	               --that will be passed to the callback. This field can be used to pass on things
                   --like message length, checksum validity, etc without re-parsing the message in
                   --processing. If message sequence IDs are not used in this protocol, always return
				   --0. If no valid packet is found, return 0 for message length and the other return
	               --values can be ignored.
}
--]]

--[[ structure of a message table:
message = {
	sequence_id,             --Message sequence identifier number (used to identify the transaction to which a response packet belongs). If not used, must be 0.
	fail_time = 123,        --System time at which waiting for a response times out
	callback = function(),  --Function called to handle response to message. Takes raw byte message stream as string argument.
	timeout = function(),   --Function called when the response has timed out (optional)
}
--]]

--Generate a new communications handler
function comm:create(send, receive, packetize)
	local new_comm = {}
	new_comm.internal = {
		port = {send = send, receive = receive},
		messages = {},
		buffer = "",
		packetize = packetize,
	}
	setmetatable(new_comm, self)
	self.__index = self
	return new_comm
end

--Send a new message over the bus. See message table format.
function comm:send(data, callback, timeout, timeout_callback, sequence_id)
	local message = {
		callback = callback,
		fail_time = os.clock() + (fail_time or 0)
		timeout_callback = timeout_callback,
		sequence_id = sequence_id or 0
	}
	table.insert(self.internal.messages, message)
	self.port.send(data)
end

--Poll the port and handle any incoming message responses
function comm:receive()
	--Add any new data to the buffer and check for newly completed packets
	self.internal.buffer = self.internal.buffer .. self.port.receive()
	local packet_length, sequence_id, packetize_data = self.internal.packetize(self.internal.buffer)
	
	--If there is a complete packet, find a message with matching sequence ID
	if packet_length > 0 then --Compare ID to pending messages
		local message_found = false
		for i,v in ipairs(self.internal.messages) do
			if v.sequence_id = sequence_id then --found one, process it
				message_found = true
				table.remove(self.internal.messages, i)
				v.callback(string.sub(self.internal.buffer, 1, packet_length), packetize_data)
				self.internal.buffer = string.sub(self.internal.buffer, packet_length + 1)
				break
			end
		end
		if not message_found then --No message found with matching sequence ID! this shouldn't happen.
			error("Packet receieved with non-matching message ID")
		end
	end
	
	--Prune expired messages
	for i,v in ipairs(self.internal.messages) do
		if v.fail_time >= os.clock() then
			table.remove(self.internal.messages, i)
			v.timeout()
		end
	end
return comm