local carbits={}

local const={
	ft_per_mi=   5280,
	in_per_m=    39.37,
	in_per_ft=   12,
	flex_factor= 1.031,
}

carbits.engine={
	maz_bp4w_ms={
		mfg=	"Mazda",
		name=	"BP-4W Mazdaspeed",
		revlim=	6500
	},
	toy_3sgte={
		mfg=	"Toyota",
		name=	"3S-GTE",
		revlim=	8000
	},
	toy_2grfe={
		mfg=	"Toyota",
		name=	"2GR-FE",
		revlim=	6500
	},
	lot_2grfe={
		mfg=	"Toyota",
		name=	"2GR-FE Lotus",
		revlim=	7200
	},
	nis_vq37vhr={
		mfg=	"Nissan",
		name=	"VQ37VHR",
		revlim= 7600
	},
	hon_j32a2={
		mfg=	"Honda",
		name=	"J32A2",
		revlim=	7200
	},
	hon_j35a8={
		mfg=	"Honda",
		name=	"J35A8",
		revlim= 6800
	},
}

carbits.trans={
	toy_e153_camry={
		mfg=	"Toyota/Aisin",
		name=	"E153 (Camry V6 3.933)",
		shafts={
			{fd=3.933, rev=3.545, 3.230, 1.913,	1.258, 0.918, 0.731}
		}
	},
	toy_e153_mr2={
		mfg=	"Toyota/Aisin",
		name=	"E153 (MR2 Turbo 4.285)",
		shafts={
			{fd=4.285, rev=3.545, 3.230, 1.913, 1.258, 0.918, 0.731}
		}
	},
	toy_ea62={
		mfg=	"Toyota/Aisin",
		name=	"EA62",
		shafts={
			{fd=3.777, 3.538, 1.913, 1.218,	0.880},
			{fd=3.238, 0.809, 0.638}
		}
	},
	toy_ea63={
		mfg=	"Toyota/Aisin",
		name=	"EA63",
		shafts={
			{fd=3.831, 3.538, 1.913, 1.218, 0.880},
			{fd=3.831, 0.809, 0.673}
		}
	},
	lot_ea62_std={
		mfg=	"Toyota/Aisin",
		name=	"EA62 Lotus standard",
		shafts={
			{fd=3.777, 3.538, 1.913, 1.218, 0.860},
			{fd=3.238, 0.790, 0.638}
		}
	},
	lot_ea62_sport={
		mfg=	"Toyota/Aisin",
		name=	"EA62 Lotus sport",
		shafts={
			{fd=3.777, 3.538, 1.913, 1.407, 1.091},
			{fd=3.238, 0.969, 0.861}
		}
	},
	maz_az6_msm={
		mfg=	"Mazda/Aisin",
		name=	"AZ6 Mazdaspeed",
		shafts={
			{fd=4.100, 3.760, 2.269, 1.645, 1.257, 1.000, 0.843}
		}
	},
}

carbits.tire={
	toy_mr2_91={
		width=205, section=60, diam=14
	},
	maz_miata_ms={
		width=205, section=40, diam=17
	},
	sizes={
		{width=205, section=60, diam=15, avail="?"},
		{width=205, section=40, diam=17, avail="?"},
		{width=285, section=40, diam=17, avail=3},
		{width=275, section=40, diam=17, avail=21},
		{width=265, section=40, diam=17, avail=2},
		{width=255, section=40, diam=17, avail=47},
		{width=255, section=45, diam=17, avail=8}
	},
}

--Determine if two tire sizes are the same - USE THIS NOT ==
function carbits.tire.compare(a, b)
	if a.width == b.width and a.section == b.section and a.diam == b.diam then
		return true
	end
	return false
end

--Calculate circumference of a tire (from standard labeling)
function carbits.tireCircumf(tire)
	local secHeight = (tire.width / 1000) * (tire.section / 100)
	local diam = (2 * secHeight) + (tire.diam / const.in_per_m)
	return diam * math.pi
end

--Find the number of revolutions a tire makes per mile
function carbits.tireRevPerMile(x)
	local in_per_mi = const.in_per_ft * const.ft_per_mi
	return (in_per_mi * const.flex_factor) / (carbits.tireCircumf(x) * const.in_per_m)
end

--Get the minimum cruising RPM at a given speed
function carbits.minRevs(trans, tire, speed)
	local ratio = math.huge
	for _, shaft in pairs(trans.shafts) do
		for _, gear in ipairs(shaft) do
			local newRatio = gear * shaft.fd
			if newRatio < ratio then ratio = newRatio end
		end
	end
	return (speed / 60) * carbits.tireRevPerMile(tire) * ratio
end

--Get a list of the output RPMS of a transmission at a given input RPM
function carbits.getTransSpeeds(inSpeed, trans)
	local result = {}
	for _, shaft in pairs(trans.shafts) do
		for k, gear in pairs(shaft) do
			if k ~= "fd" then
				table.insert(result, inSpeed / (gear * shaft.fd))
			end
		end
	end
end

--Get a list of the speed in each gear of a trans/wheel combo at a given input RPM
function carbits.getRoadSpeeds(revs, trans, tire)
	local result = {}
	local tireRevs = carbits.tireRevPerMile(tire)
	for _, shaft in pairs(trans.shafts) do
		for _, gear in ipairs(shaft) do
			table.insert(result, (revs * 60) / (gear * shaft.fd * tireRevs))
		end
		pcall(function() result[rev] = (revs * 60 * tireRevs) / (shaft.rev * shaft.fd) end)
	end
	return result
end

function carbits.displaySpeeds(eng, trans, tire)
	local result = carbits.getRoadSpeeds(eng.revlim, trans, tire)

	print(string.format("Engine:\t\t%s %s, redline %d RPM", eng.mfg, eng.name, eng.revlim))
	print(string.format("Transmission:\t%s %s", trans.mfg, trans.name))
	local tireInfo = string.format("Tire:\t\t%d/%dR%d", tire.width, tire.section, tire.diam)
	if(nil ~= tire.avail) then
		tireInfo = tireInfo .. string.format(" (%d available)", tire.avail)
	end
	print(tireInfo)

	for i,v in ipairs(result) do
		print(string.format("%d:\t%.2f", i, v))
	end
	if(nil ~= result.rev) then
		print(string.format("Rev:\t%.2f", result.rev))
	end
end

function carbits.displaySpeedDeltas(eng, trans, tire)
	local result = carbits.getRoadSpeeds(eng.revlim, trans, tire)
	print(string.format("Engine:\t\t%s %s, redline %d RPM", eng.mfg, eng.name, eng.revlim))
	print(string.format("Transmission:\t%s %s", trans.mfg, trans.name))
	local tireInfo = string.format("Tire:\t\t%d/%dR%d", tire.width, tire.section, tire.diam)
	if(nil ~= tire.avail) then
		tireInfo = tireInfo .. string.format(" (%d available)", tire.avail)
	end
	print(tireInfo)

	local lastSpeed = 0
	for i,v in ipairs(result) do
		print(string.format("%d-%d:\t%.2f", i-1, i, v - lastSpeed))
		lastSpeed = v
	end
end

return carbits
