mill={}

--All units are defined relative to millimeter
mill.units={
	inch = {
		abbrev = "in",
		factor = 25.4,
	},
	mm = {
		abbrev = "mm",
		factor = 1,
	},
}

--Decimal places in prints
mill.precision = 3

local function unitStr(value, unit)
	return string.format("%." .. tostring(mill.precision) .. "f %s", value / unit.factor, unit.abbrev)
end

function turnsStr(start, stop, turn_dist, unit)
	local turns = math.floor(stop / turn_dist) - math.floor(start / turn_dist)
	if false then turns = turns - 1 end
	return tostring(turns) .. " turns to " .. unitStr(stop % turn_dist, unit)
end

--[[ Tool to give instructions for cutting slots with curved depth profile.
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement
		} 
		unit              Display units for output
		profiles={
			n*{           Array of measurements of slot length vs depth
				depth     Depth of slot at measurement
				length    Length of slot at this depth
			}
			unit          Unit used for profile measurements
		}
		opt={
			backlash={
				value    Slop present in slot long axis on machine
				unit     Units for slop
			}
--]]
function mill.slotAbsolute(tool, unit, profiles, backlash)
	--Get native units, set backlash default value
	backlash = (backlash ~= nil) and backlash.value * backlash.unit.factor or 0
	tool = tool.width * tool.unit.factor
	--[[ Set the origin at toolpoint for start of first pass. We will internally
	     calculate with the origin as the center of the slot for simplicity, but
         the output will be written as relative to this point to ease using the
	     dials/DRO.
	--]]
	origin = 0 - ((profiles[1].length * profiles.unit.factor) - tool) / 2
	print("Set origin at surface, end of slot: " .. unitStr(origin, unit) .. " from center - approach from center to align backlash")
	local reverse_pass = true --direction of movement is toward origin point
	local x_target = 0 - origin --just going the other way, which is fine
	local next_x_target
	for i = 1, #profiles do
		x_target = ((profiles[i].length * profiles.unit.factor) - tool) / 2
		--Print stepback from last pass
		if i ~= 1 then
			print(" | Return " .. (reverse_pass and "=>" or "<=") .. " " .. unitStr((reverse_pass and x_target + backlash or x_target) - origin, unit))
		end
		--Print pass milling data
		io.write(string.format(
			"Plunge Z=%s | Cut %s %s",
			unitStr(profiles[i].depth, unit),
			reverse_pass and "=>" or "<=",
			unitStr((reverse_pass and x_target + backlash or 0 - x_target) - origin, unit)
		))
		--Calculate and print stepback if not last pass - note this also gets
		reverse_pass = not reverse_pass
	end
	io.write("\n")
end

--[[ Tool to give instructions for milling a rectangle from the outside
	Arguments:
		tool={
			width         Diameter of tool cutting surface
			unit          Units for tool measurement
		} 
		unit              Display units for output
		rect={
			x             X-axis size of rectangle
			y             Y-axis size of rectangle
			unit          Units of rectangle measurements
		}
		opts={
			stepover      Percentage of tool width to cut on each pass, as a scalar
			excess={
				value     Width of excess stock around desired profile (start distance)
				unit      Units of excess stock width
			}
			backlash_x={
				value     Slop present in X axis on machine
				unit      Units of X axis slop
			}
			backlash_y={
				value     Slop present in Y axis on machine
				unit      Units of Y axis slop
			}
		}
--]]
function mill.rectOutsideAbsolute(tool, unit, input_rect, opt)
	--Get native units, set default values
	tool = tool.width * tool.unit.factor
	local stepover = (opt.stepover ~= nil) and opt.stepover * tool or tool
	local excess = (opt.excess ~= nil) and opt.excess.value * opt.excess.unit.factor or 0
	local backlash_x = (opt.backlash_x ~= nil) and opt.backlash_x.value * opt.backlash_x.unit.factor or 0
	local backlash_y = (opt.backlash_y ~= nil) and opt.backlash_y.value * opt.backlash_y.unit.factor or 0
	local rect = {
		x = input_rect.x * input_rect.unit.factor,
		y = input_rect.y * input_rect.unit.factor,
	}
	--[[ Set the origin at toolpoint for start of cut. We will internally
	     calculate with the origin as the center of the rectangle for simplicity,
         but the output will be written as relative to this point to ease using
	     the dials/DRO.
	--]]
	local origin_x = 0 - ((rect.x + tool) / 2) - excess
	local origin_y = 0 - ((rect.y + tool) / 2) - excess
	print(string.format("Set origin to surface, X=%s, Y=%s from center - approach both axes from center to align backlash", unitStr(origin_x, unit), unitStr(origin_y, unit)))
	local x_target
	local y_target
	repeat
		--Get the next cut height
		excess = excess > stepover and excess - stepover or 0
		--Set the tool center distances
		target_x = ((rect.x + tool) / 2) + excess
		target_y = ((rect.y + tool) / 2) + excess
		io.write("Set Y=" .. unitStr(0 - target_y + backlash_y - origin_y, unit))
		io.write(" | Cut X=" .. unitStr(target_x + backlash_x - origin_x, unit))
		io.write(" | Cut Y=" .. unitStr(target_y + backlash_y - origin_y, unit))
		io.write(" | Cut X=" .. unitStr(0 - target_x - origin_x, unit))
		io.write(" | Cut Y=" .. unitStr(0 - target_y - origin_y, unit))
		io.write("\n")
	until excess == 0
	print("Cut through corner and done")
end
--OAH 4.55, stock 4.76, 0.1 surfacing

return mill
