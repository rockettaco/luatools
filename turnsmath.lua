mill = require("mill")

local function unitStr(value, unit)
	return string.format("%." .. tostring(mill.precision) .. "f %s", value / unit.factor, unit.abbrev)
end

function turnsStr(start, stop, turn_dist, unit)
	local turns = math.floor(stop / turn_dist) - math.floor(start / turn_dist)
	if false then turns = turns - 1 end
	return tostring(turns) .. " turns to " .. unitStr(stop % turn_dist, unit)
end

function testCaseTurnsStr(start, stop, turn_dist, unit, expected)
	local result = turnsStr(start, stop, turn_dist, unit)
	if result == expected then
		print("PASS")
	else
		print("FAILURE: expected \"" .. expected .. "\" but got \"" .. result .. "\"")
	end
	return
end

function testTurnsStr()
	testCaseTurnsStr( 0, 15, 20, mill.units.mm, "0 turns to 15.000 mm")
	testCaseTurnsStr(15, 0,  20, mill.units.mm, "0 turns to 0.000 mm")
	testCaseTurnsStr(19, 21, 20, mill.units.mm, "1 turns to 1.000 mm")
	testCaseTurnsStr(21, 19, 20, mill.units.mm, "-1 turns to 19.000 mm")
	testCaseTurnsStr(0,  39, 20, mill.units.mm, "1 turns to 19.000 mm")
	testCaseTurnsStr(39, 12, 20, mill.units.mm, "-1 turns to 12.000 mm")
	testCaseTurnsStr(15, 45, 20, mill.units.mm, "2 turns to 5.000 mm")
	testCaseTurnsStr(45, 15, 20, mill.units.mm, "-2 turns to 15.000 mm")
end
